import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;
import 'package:http/http.dart' as http;
import 'dart:convert';

class PengaturanPage extends StatefulWidget {
  @override
  _PengaturanPageState createState() => _PengaturanPageState();
}

class _PengaturanPageState extends State<PengaturanPage> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var value = 0.1;
  var language = 'indonesia';
  var refrain = true;
  var jenis_audio = 'Inst + Vocal';
  var pengulangan_lagu = 'Repeat All';
  var pemutaran_bergilir = false;
  var lagu_utama = 'Album A';
  var mobile_data = 'No Media';
  var dengan_wifi = 'No Media';
  var jenis_font = 'Roboto';

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
      if (prefs.getString('jenis_font') == null) {
        jenis_font = 'Roboto';
      } else {
        jenis_font = prefs.getString('jenis_font');
      }
      if (prefs.getBool('refrain') == null) {
        refrain = true;
      } else {
        refrain = prefs.getBool('refrain');
      }
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, bahasa) {
    var styleFont;
    if (fontstyle == 'Roboto') {
      styleFont = GoogleFonts.roboto(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Average') {
      styleFont = GoogleFonts.average(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Aleo') {
      styleFont = GoogleFonts.aleo(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Delius') {
      styleFont = GoogleFonts.delius(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    } else if (fontstyle == 'Vollkorn') {
      styleFont = GoogleFonts.vollkorn(
          textStyle: TextStyle(
        fontSize: ukuranfont == 0 ? null : ukuranfont + (5 * value),
        fontFamily: fontstyle,
      ));
    }
    return Text(
      bahasa == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: styleFont,
    );
  }

  getLaguUtama() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      lagu_utama = prefs.getString('lagu_utama') == null
          ? 'Album A'
          : prefs.getString('lagu_utama');
      mobile_data = prefs.getString('mobile_data') == null
          ? 'No Media'
          : prefs.getString('mobile_data');
      dengan_wifi = prefs.getString('dengan_wifi') == null
          ? 'No Media'
          : prefs.getString('dengan_wifi');
    });
    Future.delayed(Duration(seconds: 1), () {
      getLaguUtama();
    });
  }

  List album;

  Future getAlbum() async {
    http.Response response = await http.get(apiMaster + '/album', headers: {
      'Accept': 'application/json',
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> _album = json.decode(response.body);
      album = _album["albums"];
    });
  }

  loadAudioSetting() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      jenis_audio = prefs.getString('jenis_audio') == null
          ? 'Inst + Vocal'
          : prefs.getString('jenis_audio');
      pengulangan_lagu = prefs.getString('pengulangan_lagu') == null
          ? 'Repeat All'
          : prefs.getString('pengulangan_lagu');
      pemutaran_bergilir = prefs.getBool('pemutaran_bergilir') == null
          ? false
          : prefs.getBool('pemutaran_bergilir');
    });
  }

  @override
  void initState() {
    super.initState();
    ukuranText();
    loadAudioSetting();
    getAlbum();
    getLaguUtama();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: tampil_text('pengaturan', 0, jenis_font, language),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                      builder: (context, setState) {
                        setState(() {
                          lagu_utama = lagu_utama;
                        });
                        return SimpleDialog(
                          title: const Text('Lagu Utama'),
                          children: <Widget>[
                            Container(
                              width: double.maxFinite,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: album == null ? 0 : album.length,
                                  itemBuilder: (context, index) {
                                    return RadioListTile(
                                      value: album[index]['album_name'],
                                      groupValue: lagu_utama,
                                      onChanged: (value_lagu_utama) async {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        setState(() {
                                          lagu_utama = value_lagu_utama;
                                          prefs.setString(
                                              'lagu_utama', lagu_utama);
                                          prefs.commit();
                                          Navigator.pop(context);
                                        });
                                      },
                                      title: Text(album[index]['album_name']),
                                    );
                                  }),
                            ),
                          ],
                        );
                      },
                    );
                  },
                );
              },
              child: ListTile(
                title: tampil_text('lagu_utama', 15, jenis_font, language),
                subtitle: Text(
                  lagu_utama == null ? '...' : lagu_utama,
                  style: TextStyle(fontSize: 15 + (5 * value)),
                ),
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "Audio",
                style: TextStyle(
                  fontSize: 16 + (5 * value),
                  color: Colors.deepOrange,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title: const Text('Audio Type'),
                      children: <Widget>[
                        RadioListTile(
                          value: 'Inst + Vocal',
                          groupValue: jenis_audio,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_audio = value;
                              prefs.setString('jenis_audio', jenis_audio);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Inst + Vocal'),
                        ),
                        RadioListTile(
                          value: 'Minus One',
                          groupValue: jenis_audio,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_audio = value;
                              prefs.setString('jenis_audio', jenis_audio);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Minus One'),
                        ),
                      ],
                    );
                  },
                );
              },
              child: ListTile(
                title: tampil_text('jenis_audio', 15, jenis_font, language),
                subtitle: Text(
                  jenis_audio,
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title: const Text('Song Repeat'),
                      children: <Widget>[
                        RadioListTile(
                          value: 'Repeat All',
                          groupValue: pengulangan_lagu,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              pengulangan_lagu = value;
                              prefs.setString(
                                  'pengulangan_lagu', pengulangan_lagu);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.repeat),
                              SizedBox(
                                width: 5,
                              ),
                              Text('Repeat All'),
                            ],
                          ),
                        ),
                        RadioListTile(
                          value: 'Shuffle',
                          groupValue: pengulangan_lagu,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              pengulangan_lagu = value;
                              prefs.setString(
                                  'pengulangan_lagu', pengulangan_lagu);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.shuffle),
                              SizedBox(
                                width: 5,
                              ),
                              Text('Shuffle'),
                            ],
                          ),
                        ),
                        RadioListTile(
                          value: 'Repeat One',
                          groupValue: pengulangan_lagu,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              pengulangan_lagu = value;
                              prefs.setString(
                                  'pengulangan_lagu', pengulangan_lagu);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.repeat_one),
                              SizedBox(
                                width: 5,
                              ),
                              Text('Repeat One'),
                            ],
                          ),
                        ),
                        RadioListTile(
                          value: 'None',
                          groupValue: pengulangan_lagu,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              pengulangan_lagu = value;
                              prefs.setString(
                                  'pengulangan_lagu', pengulangan_lagu);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.link_off),
                              SizedBox(
                                width: 5,
                              ),
                              Text('None'),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
              child: ListTile(
                title:
                    tampil_text('pengulangan_lagu', 15, jenis_font, language),
                subtitle: Text(
                  pengulangan_lagu,
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            ListTile(
              title:
                  tampil_text('pemutaran_bergilir', 15, jenis_font, language),
              subtitle: tampil_text(
                  'pemutaran_jenis_audio', 14, jenis_font, language),
              trailing: Switch(
                value: pemutaran_bergilir,
                onChanged: (newVal) async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  setState(() {
                    pemutaran_bergilir = newVal;
                    prefs.setBool('pemutaran_bergilir', pemutaran_bergilir);
                    prefs.commit();
                  });
                },
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "Text",
                style: TextStyle(
                  fontSize: 16 + (5 * value),
                  color: Colors.deepOrange,
                ),
              ),
            ),
            ListTile(
              title: tampil_text('ukuran_text', 15, jenis_font, language),
              subtitle: Slider(
                onChanged: (double value_) async {
                  setState(() {
                    value = value_;
                  });
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.setDouble('ukuran_text', value_);
                  prefs.commit();
                },
                value: value,
                activeColor: Colors.amber[900],
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title: const Text('Refrain setiap ayat'),
                      children: <Widget>[
                        RadioListTile(
                          value: true,
                          groupValue: refrain,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              refrain = value;
                              prefs.setBool('refrain', refrain);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Active'),
                        ),
                        RadioListTile(
                          value: false,
                          groupValue: refrain,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              refrain = value;
                              prefs.setBool('refrain', refrain);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Non-Active'),
                        ),
                      ],
                    );
                  },
                );
              },
              child: ListTile(
                title: tampil_text('refrain', 15, jenis_font, language),
                subtitle: tampil_text('teks_refrain', 14, jenis_font, language),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title: const Text('Jenis Font'),
                      children: <Widget>[
                        RadioListTile(
                          value: 'Roboto',
                          groupValue: jenis_font,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_font = value;
                              prefs.setString('jenis_font', jenis_font);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Roboto'),
                        ),
                        RadioListTile(
                          value: 'Average',
                          groupValue: jenis_font,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_font = value;
                              prefs.setString('jenis_font', jenis_font);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Average'),
                        ),
                        RadioListTile(
                          value: 'Aleo',
                          groupValue: jenis_font,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_font = value;
                              prefs.setString('jenis_font', jenis_font);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Aleo'),
                        ),
                        RadioListTile(
                          value: 'Delius',
                          groupValue: jenis_font,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_font = value;
                              prefs.setString('jenis_font', jenis_font);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Delius'),
                        ),
                        RadioListTile(
                          value: 'Vollkorn',
                          groupValue: jenis_font,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              jenis_font = value;
                              prefs.setString('jenis_font', jenis_font);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Vollkorn'),
                        ),
                      ],
                    );
                  },
                );
              },
              child: ListTile(
                title: tampil_text('jenis_font', 15, jenis_font, language),
                subtitle: Text(
                  jenis_font,
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "Auto Download",
                style: TextStyle(
                  fontSize: 16 + (5 * value),
                  color: Colors.deepOrange,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                      builder: (context, setState) {
                        setState(() {
                          lagu_utama = lagu_utama;
                        });
                        return SimpleDialog(
                          children: <Widget>[
                            Container(
                              width: double.maxFinite,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: album == null ? 0 : album.length,
                                  itemBuilder: (context, index) {
                                    return RadioListTile(
                                      value: album[index]['album_name'],
                                      groupValue: mobile_data,
                                      onChanged: (value_mobile_data) async {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        setState(() {
                                          mobile_data = value_mobile_data;
                                          prefs.setString(
                                              'mobile_data', mobile_data);
                                          prefs.commit();
                                          Navigator.pop(context);
                                        });
                                      },
                                      title: Text(album[index]['album_name']),
                                    );
                                  }),
                            ),
                          ],
                        );
                      },
                    );
                  },
                );
              },
              child: ListTile(
                title:
                    tampil_text('ketika_menggunakan', 15, jenis_font, language),
                subtitle: Text(
                  mobile_data,
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                      builder: (context, setState) {
                        setState(() {
                          lagu_utama = lagu_utama;
                        });
                        return SimpleDialog(
                          children: <Widget>[
                            Container(
                              width: double.maxFinite,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: album == null ? 0 : album.length,
                                  itemBuilder: (context, index) {
                                    return RadioListTile(
                                      value: album[index]['album_name'],
                                      groupValue: dengan_wifi,
                                      onChanged: (value_dengan_wifi) async {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        setState(() {
                                          dengan_wifi = value_dengan_wifi;
                                          prefs.setString(
                                              'dengan_wifi', dengan_wifi);
                                          prefs.commit();
                                          Navigator.pop(context);
                                        });
                                      },
                                      title: Text(album[index]['album_name']),
                                    );
                                  }),
                            ),
                          ],
                        );
                      },
                    );
                  },
                );
              },
              child: ListTile(
                title:
                    tampil_text('ketika_terkoneksi', 15, jenis_font, language),
                subtitle: Text(
                  dengan_wifi,
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () {
                showChooser();
              },
              child: ListTile(
                title: tampil_text('tema', 15, jenis_font, language),
                subtitle: Text(
                  Theme.of(context).brightness.toString() == 'Brightness.dark'
                      ? 'Gelap'
                      : 'Default',
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
            Divider(),
            GestureDetector(
              onTap: () {
                showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      title: const Text('Select Language'),
                      children: <Widget>[
                        RadioListTile(
                          value: 'indonesia',
                          groupValue: language,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              language = value;
                              prefs.setString('language', language);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('Bahasa Indonesia'),
                        ),
                        RadioListTile(
                          value: 'english',
                          groupValue: language,
                          onChanged: (value) async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            setState(() {
                              language = value;
                              prefs.setString('language', language);
                              prefs.commit();
                              Navigator.pop(context);
                            });
                          },
                          title: const Text('English'),
                        ),
                      ],
                    );
                  },
                );
              },
              child: ListTile(
                title: tampil_text('bahasa_aplikasi', 15, jenis_font, language),
                subtitle: Text(
                  language.capitalize(),
                  style: TextStyle(fontSize: 14 + (5 * value)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showChooser() {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return BrightnessSwitcherDialog(
          onSelectedTheme: (Brightness brightness) {
            DynamicTheme.of(context).setBrightness(brightness);
          },
        );
      },
    );
  }
}

class BrightnessSwitcherDialog extends StatelessWidget {
  const BrightnessSwitcherDialog({Key key, this.onSelectedTheme})
      : super(key: key);

  final ValueChanged<Brightness> onSelectedTheme;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: const Text('Select Theme'),
      children: <Widget>[
        RadioListTile<Brightness>(
          value: Brightness.light,
          groupValue: Theme.of(context).brightness,
          onChanged: (Brightness value) {
            onSelectedTheme(Brightness.light);
          },
          title: const Text('Default'),
        ),
        RadioListTile<Brightness>(
          value: Brightness.dark,
          groupValue: Theme.of(context).brightness,
          onChanged: (Brightness value) {
            onSelectedTheme(Brightness.dark);
          },
          title: const Text('Gelap'),
        ),
      ],
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
