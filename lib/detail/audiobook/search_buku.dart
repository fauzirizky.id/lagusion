import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/bloc/audio_book_model.dart';
import 'package:music_mulai/detail/audiobook/list_audiobook.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

import '../../main.dart';

class SearchBukuPage extends StatefulWidget {
  @override
  _SearchBukuPageState createState() => _SearchBukuPageState();
}

class _SearchBukuPageState extends State<SearchBukuPage>
    with SingleTickerProviderStateMixin {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  List<dynamic> audioBook;

  bool downloading = false;
  var progressString = "";
  var indexDownload;
  var _indexDownload;
  var value = 0.1;
  var language;

  List laguSion;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : color == null
              ? TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                )
              : TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                  color: color,
                ),
    );
  }

  Future getAudioBook() async {
    http.Response response =
        await http.post(apiMaster + '/audio-book/search', headers: {
      'Accept': 'application/json',
    }, body: {
      'search': _searchQueryController.text
    });
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      Map<String, dynamic> _audioBook = json.decode(response.body);
      audioBook = _audioBook["audio_books"];
      print(audioBook);
    });
  }

  void initState() {
    getAudioBook();
    super.initState();
  }

  var recentSongs;

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "Search query";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Container(
          margin: EdgeInsets.only(bottom: 15),
          child: TextFormField(
            controller: _searchQueryController,
            autofocus: true,
            decoration: InputDecoration(
                hintText: "Cari...",
                hintStyle: TextStyle(color: Colors.white60),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(top: 10.0)),
            style: TextStyle(color: Colors.white, fontSize: 20.0),
            onChanged: (query) => getAudioBook(),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 18.0),
            child: Icon(
              Icons.mic,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: audioBook == null ? 3 : audioBook.length,
              itemBuilder: (_, int index) {
                if (audioBook == null) {
                  return SongSkeleton();
                } else {
                  return audioBook[index]['parts'].toString() != '[]'
                      ? GestureDetector(
                          onTap: () {
                            // Provider.of<HomeModel>(context).stop();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListAudioBook(
                                        audioBookModel: AudioBookModel.fromJson(audioBook[index]),
                                      )),
                            );
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Image.network(
                                      audioBook[index]['thumbnail'],
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Flexible(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            audioBook[index]['audio_book_name'],
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                            .brightness
                                                            .toString() ==
                                                        'Brightness.dark'
                                                    ? Colors.white
                                                    : Colors.black,
                                                fontSize: 16 + (5 * value),
                                                fontFamily: 'Roboto'),
                                          ),
                                          SizedBox(height: 5.0),
                                          Text(
                                            audioBook[index]['parts'][0]
                                                ['title'],
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                          .brightness
                                                          .toString() ==
                                                      'Brightness.dark'
                                                  ? Colors.white
                                                  : Colors.black
                                                      .withOpacity(0.5),
                                              fontSize: 16 + (5 * value),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                '',
                                                style: TextStyle(
                                                  fontSize: 10 + (5 * value),
                                                ),
                                              ),
                                              Container(
                                                transform:
                                                    Matrix4.translationValues(
                                                        -8, 0, 0),
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: Theme.of(context)
                                                              .brightness
                                                              .toString() ==
                                                          'Brightness.dark'
                                                      ? Colors.white
                                                      : Colors.black
                                                          .withOpacity(0.6),
                                                  size: 25.0,
                                                ),
                                              )
                                            ],
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Container(
                                                transform:
                                                    Matrix4.translationValues(
                                                        8.0, 0.0, 0.0),
                                                child: PopupMenuButton<String>(
                                                  child: Icon(Icons.more_vert,
                                                      size: 18),
                                                  onSelected: (choice) async {},
                                                  itemBuilder:
                                                      (BuildContext context) {
                                                    return MenuMoreSearch
                                                        .choices
                                                        .map((String choice) {
                                                      return PopupMenuItem<
                                                          String>(
                                                        value: choice,
                                                        child: Text(choice),
                                                      );
                                                    }).toList();
                                                  },
                                                ),
                                              ),
                                              audioBook[index]['parts']
                                                          .toString() ==
                                                      '[]'
                                                  ? Container()
                                                  : Text(
                                                      audioBook[index]['parts']
                                                              [0]['duration']
                                                          .replaceAll(
                                                              new RegExp(
                                                                  r"\s+\b|\b\s"),
                                                              ""),
                                                      style: TextStyle(
                                                        fontSize:
                                                            14 + (5 * value),
                                                      ),
                                                    )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSearchField() {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: TextField(
        controller: _searchQueryController,
        autofocus: true,
        decoration: InputDecoration(
            hintText: "Cari...",
            hintStyle: TextStyle(color: Colors.white60),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            contentPadding: EdgeInsets.only(top: 10.0)),
        style: TextStyle(color: Colors.white, fontSize: 20.0),
        onChanged: (query) => updateSearchQuery,
      ),
    );
  }

  void _startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }
}

class MenuMoreSearch {
  static const String Unduh = 'Unduh';

  static const List<String> choices = <String>[
    Unduh,
    // LaguUtama,
  ];
}
