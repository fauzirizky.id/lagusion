import 'package:flutter/material.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/detail/lagu/search_numpad.dart';
import 'dart:async';
import 'dart:convert';
import 'package:music_mulai/locale/en.dart' as en;
import 'package:music_mulai/locale/id.dart' as id;

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailFavorit extends StatefulWidget {
  final tipe;
  DetailFavorit(this.tipe);
  @override
  _DetailFavoritState createState() => _DetailFavoritState();
}

class _DetailFavoritState extends State<DetailFavorit> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';

  List<dynamic> laguSion;

  var value = 0.1;
  var language;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
    });
    Future.delayed(Duration(seconds: 1), () {
      ukuranText();
    });
  }

  tampil_text(var_text, ukuranfont, fontstyle, fontweight, color) {
    return Text(
      language == 'indonesia' ? id.indonesia[var_text] : en.english[var_text],
      style: ukuranfont == 0
          ? TextStyle(fontFamily: fontstyle)
          : color == null
              ? TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                )
              : TextStyle(
                  fontSize: ukuranfont + (5 * value),
                  fontWeight: fontweight,
                  fontFamily: fontstyle,
                  color: color,
                ),
    );
  }

  Future getDataLagu() async {
    if (widget.tipe == 'trending') {
      http.Response response =
          await http.get(apiMaster + '/song/trending', headers: {
        'Accept': 'application/json',
      });
      await Future.delayed(Duration(milliseconds: 500));
      setState(() {
        Map<String, dynamic> laguDataMentah = json.decode(response.body);
        laguSion = laguDataMentah["songs"];
        print(laguSion);
      });
    } else {
      http.Response response = await http.get(apiMaster + '/song', headers: {
        'Accept': 'application/json',
      });
      await Future.delayed(Duration(milliseconds: 500));
      setState(() {
        Map<String, dynamic> laguDataMentah = json.decode(response.body);
        laguSion = laguDataMentah["songs"];
        print(laguSion);
      });
    }
  }

  StreamBuilder<List<Favorit>> favoritSong(BuildContext context, id, index) {
    final database = Provider.of<AppDatabase>(context);
    return StreamBuilder(
      stream: database.watchWhereFavorits(id),
      builder: (context, AsyncSnapshot<List<Favorit>> snapshot) {
        final songs = snapshot.data ?? List();
        return Container(
          padding: EdgeInsets.only(right: 10),
          child: songs.toString() == '[]'
              ? Container()
              : Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Provider.of<HomeModel>(context).stop();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailMusicApp(
                                    indexCurrentPlay: index,
                                    listMusic: laguSion
                                        .map((e) => SongModel.fromJson(e))
                                        .toList()
                                  )),
                        );
                      },
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.network(
                                  laguSion[index]['thumbnail'],
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        laguSion[index]['title'],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Theme.of(context)
                                                        .brightness
                                                        .toString() ==
                                                    'Brightness.dark'
                                                ? Colors.white
                                                : Colors.black,
                                            fontSize: 16 + (5 * value),
                                            fontFamily: 'Roboto'),
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        laguSion[index]['artist'],
                                        style: TextStyle(
                                          color: Theme.of(context)
                                                      .brightness
                                                      .toString() ==
                                                  'Brightness.dark'
                                              ? Colors.white
                                              : Colors.black.withOpacity(0.5),
                                          fontSize: 16 + (5 * value),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'C 4/4',
                                            style: TextStyle(
                                              fontSize: 10 + (5 * value),
                                            ),
                                          ),
                                          Container(
                                            transform:
                                                Matrix4.translationValues(
                                                    -8, 0, 0),
                                            child: Icon(
                                              Icons.play_arrow,
                                              color: Theme.of(context)
                                                          .brightness
                                                          .toString() ==
                                                      'Brightness.dark'
                                                  ? Colors.white
                                                  : Colors.black
                                                      .withOpacity(0.6),
                                              size: 25.0,
                                            ),
                                          )
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                            transform:
                                                Matrix4.translationValues(
                                                    8.0, 0.0, 0.0),
                                            child: PopupMenuButton<String>(
                                              child: Icon(Icons.more_vert,
                                                  size: 18),
                                              onSelected: choiceAction,
                                              itemBuilder:
                                                  (BuildContext context) {
                                                return MenuMore.choices
                                                    .map((String choice) {
                                                  return PopupMenuItem<String>(
                                                    value: choice,
                                                    child: Text(choice),
                                                  );
                                                }).toList();
                                              },
                                            ),
                                          ),
                                          Text(
                                            laguSion[index]['duration']
                                                .replaceAll(
                                                    new RegExp(r"\s+\b|\b\s"),
                                                    ""),
                                            style: TextStyle(
                                              fontSize: 14 + (5 * value),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                  ],
                ),
        );
      },
    );
  }

  void initState() {
    super.initState();
    ukuranText();
    getDataLagu();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.tipe.toString().capitalize(),
          overflow: TextOverflow.ellipsis,
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 12, top: 10),
            child: Icon(Icons.search),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 0, top: 10),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: laguSion == null ? 0 : laguSion.length,
              itemBuilder: (_, int index) {
                var id = laguSion[index]['id'];
                return favoritSong(context, id, index);
              },
            ),
          ],
        ),
      ),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    } else if (choice == MenuMore.LaguUtama) {
      print('Jadikan Lagu Utama');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String LaguUtama = 'Jadikan Lagu Utama';

  static const List<String> choices = <String>[
    Unduh,
    LaguUtama,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
