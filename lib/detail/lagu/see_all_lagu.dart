import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/database/moor_database.dart';
import 'package:music_mulai/detail/lagu/detail_music.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/detail/lagu/search_numpad.dart';
import 'package:music_mulai/detail/search.dart';
import 'dart:async';
import 'dart:convert';

import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SeeAllLagu extends StatefulWidget {
  final tipe;
  SeeAllLagu(this.tipe);
  @override
  _SeeAllLaguState createState() => _SeeAllLaguState();
}

class _SeeAllLaguState extends State<SeeAllLagu> {
  var apiMaster = "http://lagu-sion.demibangsa.com/api";
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';
  var flume =
      'https://i.scdn.co/image/8d84f7b313ca9bafcefcf37d4e59a8265c7d3fff';
  var language;
  var value = 0.1;

  ukuranText() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // print(prefs.getDouble('ukuran_text'));
    setState(() {
      if (prefs.getDouble('ukuran_text') == null) {
        value = 0.1;
      } else {
        value = prefs.getDouble('ukuran_text');
      }
      if (prefs.getString('language') == null) {
        language = 'indonesia';
      } else {
        language = prefs.getString('language');
      }
    });
  }

  List<dynamic> laguSion;

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          var nilai = '';
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              child: Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.centerRight,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                nilai,
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Theme.of(context)
                                                .brightness
                                                .toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black),
                              ),
                              Divider()
                            ],
                          ),
                        ),
                        FlatButton(
                          child: Icon(
                            Icons.backspace,
                            color: Theme.of(context).brightness.toString() ==
                                    'Brightness.dark'
                                ? Colors.white
                                : Colors.black.withOpacity(0.5),
                          ),
                          onPressed: () {
                            setState(() {
                              if (nilai.length > 0) {
                                nilai = nilai.substring(0, nilai.length - 1);
                              }
                            });
                          },
                          onLongPress: () {
                            setState(() {
                              nilai = '';
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '1';
                                  });
                                },
                                child: Container(
                                  child: Text('1',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '2';
                                  });
                                },
                                child: Container(
                                  child: Text('2',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '3';
                                  });
                                },
                                child: Container(
                                  child: Text('3',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '4';
                                  });
                                },
                                child: Container(
                                  child: Text('4',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '5';
                                  });
                                },
                                child: Container(
                                  child: Text('5',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '6';
                                  });
                                },
                                child: Container(
                                  child: Text('6',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '7';
                                  });
                                },
                                child: Container(
                                  child: Text('7',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '8';
                                  });
                                },
                                child: Container(
                                  child: Text('8',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  setState(() {
                                    nilai = nilai + '9';
                                  });
                                },
                                child: Container(
                                  child: Text('9',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 30)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Text('ABC',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        nilai = nilai + '0';
                                      });
                                    },
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 30),
                                      child: Text('0',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30)),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SearchNumpad(
                                                    search: nilai,
                                                  )));
                                    },
                                    child: Container(
                                      child: Icon(Icons.search, size: 30),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }

  Future getDataLagu() async {
    if (widget.tipe == 'trending') {
      http.Response response = await http.get(apiMaster + '/song', headers: {
        'Accept': 'application/json',
      });
      await Future.delayed(Duration(milliseconds: 500));
      setState(() {
        Map<String, dynamic> laguDataMentah = json.decode(response.body);
        laguSion = laguDataMentah["songs"];
        print(laguSion);
      });
    } else {
      http.Response response = await http.get(apiMaster + '/song', headers: {
        'Accept': 'application/json',
      });
      await Future.delayed(Duration(milliseconds: 500));
      setState(() {
        Map<String, dynamic> laguDataMentah = json.decode(response.body);
        laguSion = laguDataMentah["songs"];
        print(laguSion);
      });
    }
  }

  void initState() {
    super.initState();
    getDataLagu();
    ukuranText();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.tipe.toString().capitalize(),
          overflow: TextOverflow.ellipsis,
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchPage()));
            },
            icon: Icon(Icons.search),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 10, right: 0, top: 10),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  itemCount: laguSion == null ? 10 : laguSion.length,
                  itemBuilder: (context, int index) {
                    return Container(
                      padding: EdgeInsets.only(right: 10),
                      child: laguSion == null
                          ? SongSkeleton()
                          : Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    // Provider.of<HomeModel>(context).stop();
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailMusicApp(
                                                indexCurrentPlay: index,
                                                listMusic: laguSion
                                                    .map((e) =>
                                                        SongModel.fromJson(e))
                                                    .toList()
                                              )),
                                    );
                                  },
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.grey.withOpacity(0.4),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                            child: Image.network(
                                              laguSion[index]['thumbnail'],
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Flexible(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    (laguSion[index]['index'])
                                                            .toString() +
                                                        '. ' +
                                                        laguSion[index]
                                                            ['title'],
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                                    .brightness
                                                                    .toString() ==
                                                                'Brightness.dark'
                                                            ? Colors.white
                                                            : Colors.black,
                                                        fontSize: 16.5,
                                                        fontFamily: 'Roboto'),
                                                  ),
                                                  SizedBox(height: 5.0),
                                                  Text(
                                                    laguSion[index]['artist'],
                                                    style: TextStyle(
                                                      color: Theme.of(context)
                                                                  .brightness
                                                                  .toString() ==
                                                              'Brightness.dark'
                                                          ? Colors.white
                                                          : Colors.black
                                                              .withOpacity(0.5),
                                                      fontSize: 16.5,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        'C 4/4',
                                                        style: TextStyle(
                                                            fontSize: 10),
                                                      ),
                                                      Container(
                                                        transform: Matrix4
                                                            .translationValues(
                                                                -8, 0, 0),
                                                        child: Icon(
                                                          Icons.play_arrow,
                                                          color: Theme.of(context)
                                                                      .brightness
                                                                      .toString() ==
                                                                  'Brightness.dark'
                                                              ? Colors.white
                                                              : Colors.black
                                                                  .withOpacity(
                                                                      0.6),
                                                          size: 25.0,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Column(
                                                    children: <Widget>[
                                                      Container(
                                                        transform: Matrix4
                                                            .translationValues(
                                                                8.0, 0.0, 0.0),
                                                        child: PopupMenuButton<
                                                            String>(
                                                          child: Icon(
                                                              Icons.more_vert,
                                                              size: 18),
                                                          onSelected:
                                                              (choice) async {
                                                            if (choice ==
                                                                MenuMore
                                                                    .Share) {
                                                              Share.share('Dengarkan lagu ' +
                                                                  laguSion[
                                                                          index]
                                                                      [
                                                                      "title"] +
                                                                  ' di aplikasi Lagu Sion Plus');
                                                            }
                                                          },
                                                          itemBuilder:
                                                              (BuildContext
                                                                  context) {
                                                            return MenuMore
                                                                .choices
                                                                .map((String
                                                                    choice) {
                                                              return PopupMenuItem<
                                                                  String>(
                                                                value: choice,
                                                                child: Text(
                                                                    choice),
                                                              );
                                                            }).toList();
                                                          },
                                                        ),
                                                      ),
                                                      Text(laguSion[index]
                                                              ['duration']
                                                          .replaceAll(
                                                              new RegExp(
                                                                  r"\s+\b|\b\s"),
                                                              ""))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(),
                              ],
                            ),
                    );
                  }),
            ),
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //     onPressed: () {
      //       _displayDialog(context);
      //     },
      //     backgroundColor: Colors.deepOrange,
      //     child: Icon(
      //       Icons.dialpad,
      //       color: Colors.white,
      //       size: 33,
      //     )),
    );
  }

  void choiceAction(String choice) {
    if (choice == MenuMore.Unduh) {
      print('Unduh');
    } else if (choice == MenuMore.Share) {
      print('Share');
    }
  }
}

class MenuMore {
  static const String Unduh = 'Unduh';
  static const String Share = 'Bagikan';

  static const List<String> choices = <String>[
    Unduh,
    Share,
  ];
}

class SongSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(10)),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 15,
                          width: 200,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 10,
                          width: 150,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 20,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Container(
                              transform: Matrix4.translationValues(-8, 0, 0),
                              child: Icon(
                                Icons.play_arrow,
                                color:
                                    Theme.of(context).brightness.toString() ==
                                            'Brightness.dark'
                                        ? Colors.white
                                        : Colors.black.withOpacity(0.6),
                                size: 25.0,
                              ),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              transform:
                                  Matrix4.translationValues(8.0, 0.0, 0.0),
                              child: PopupMenuButton<String>(
                                child: Icon(Icons.more_vert, size: 18),
                                itemBuilder: (BuildContext context) {
                                  return MenuMore.choices.map((String choice) {
                                    return PopupMenuItem<String>(
                                      value: choice,
                                      child: Text(choice),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            Container(
                              height: 10,
                              width: 30,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.4),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}
