import 'package:flutter/material.dart';

import '../bottomsheet.dart';

// class DetailMusicNoteApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(body: DetailMusicNote());
//   }
// }

class DetailMusicNote extends StatefulWidget {
  @override
  _DetailMusicNoteState createState() => _DetailMusicNoteState();
}

class _DetailMusicNoteState extends State<DetailMusicNote> {
  var martinGarrix =
      'https://c1.staticflickr.com/2/1841/44200429922_d0cbbf22ba_b.jpg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(65.0), // here the desired height
        child: AppBar(
          leading: Padding(
              padding: EdgeInsets.only(top: 10),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: BackButton())),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 35.0,
                      width: 35.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          martinGarrix,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 5),
                  width: MediaQuery.of(context).size.width - 225,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "9. Hai Kristen Nyanyilah",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 16.5, fontFamily: 'Roboto'),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        "Theodore Baker, 1917 4/4 C = Do",
                        style: TextStyle(
                          fontSize: 13.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 8, top: 10),
              child: Icon(Icons.favorite),
            ),
            Padding(
              padding: EdgeInsets.only(right: 8, top: 10),
              child: Icon(Icons.translate),
            ),
            Padding(
              padding: EdgeInsets.only(right: 12, top: 10),
              child: Icon(Icons.more_vert),
            ),
          ],
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 68 / 100,
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(martinGarrix),
                          fit: BoxFit.cover)),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              showBottomSheet(
                  context: context, builder: (context) => BottomSheetWidget());
            },
            child: Center(
              child: Container(
                transform: Matrix4.translationValues(0.0, 10.0, 0.0),
                child: Icon(
                  Icons.keyboard_arrow_up,
                  color: Colors.deepOrange,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '2:10',
                  style: TextStyle(
                      color: Theme.of(context).brightness.toString() ==
                              'Brightness.dark'
                          ? Colors.white
                          : Colors.black.withOpacity(0.7)),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 125,
                  child: Slider(
                    onChanged: (double value) {},
                    value: 0.2,
                    activeColor: Colors.amber[900],
                  ),
                ),
                Text('-03:56',
                    style: TextStyle(
                        color: Theme.of(context).brightness.toString() ==
                                'Brightness.dark'
                            ? Colors.white
                            : Colors.black.withOpacity(0.7)))
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  Icons.repeat,
                  color: Theme.of(context).brightness.toString() ==
                          'Brightness.dark'
                      ? Colors.white
                      : Colors.black54,
                  size: 32.0,
                ),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.skip_previous,
                      color: Theme.of(context).brightness.toString() ==
                              'Brightness.dark'
                          ? Colors.white
                          : Colors.black54,
                      size: 42.0,
                    ),
                    SizedBox(
                      width: 32,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.amber[900],
                          borderRadius: BorderRadius.circular(50.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.play_arrow,
                          size: 32.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 32,
                    ),
                    Icon(
                      Icons.skip_next,
                      color: Theme.of(context).brightness.toString() ==
                              'Brightness.dark'
                          ? Colors.white
                          : Colors.black54,
                      size: 42.0,
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.more_horiz,
                    color: Theme.of(context).brightness.toString() ==
                            'Brightness.dark'
                        ? Colors.white
                        : Colors.black54,
                    size: 32.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
