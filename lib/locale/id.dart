var indonesia = {
  //halaman home
  "see_all": "Lihat semua",
  "recent_song": "Lagu terbaru",
  "recent_book": "Buku terbaru",
  "favorit": "Favorit",
  "unduhan": "Unduhan",
  "unduh": "Unduh",
  "tersimpan": "Tersimpan",
  "lagu": "Lagu",
  "buku": "Buku",
  "trending": "Trending",
  "playlist": "Playlist",
  "audiobook": "Audio Book",

  //sidebar
  "pesan": "Pesan",
  "kirim_feedback": "Kirim Feedback",
  "feedback": "Feedback",
  "bagikan_aplikasi": "Bagikan Aplikasi",
  "tentang_aplikasi": "Tentang Aplikasi",

  //playlist
  "recently_added": "Terakhir Ditambahkan",

  //Buku
  "ibadah": "Ibadah",

  //search
  "cari": "Cari",
  "judul": "JUDUL",
  "lirik": "LIRIK",
  "all_judul": "ALL JUDUL",
  "all_lirik": "ALL LIRIK",

  // halaman pengaturan
  "pengaturan": "Pengaturan",
  "lagu_utama": "Lagu Utama",
  "jenis_audio": "Jenis Audio",
  "pengulangan_lagu": "Pengulangan Lagu",
  "pemutaran_bergilir": "Pemutaran Bergilir",
  "pemutaran_jenis_audio":
      "Pemutaran jenis audio tidak akan berubah ketika mengulang lagu yang sama",
  "ukuran_text": "Ukuran Text",
  "refrain": "Refrain di setiap ayat",
  "teks_refrain": "Teks Refrain tidak akan diulang di setiap akhir ayat",
  "jenis_font": "Jenis Font",
  "ketika_menggunakan": "Ketika menggunakan mobile data",
  "ketika_terkoneksi": "Ketika terkoneksi dengan Wi-Fi",
  "tema": "Tema",
  "bahasa_aplikasi": "Bahasa Aplikasi"
};
