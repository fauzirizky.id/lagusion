import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/search_model/search_ibadah_model.dart';

Future<SearchIbadahModel> fetchSearchIbadahModel(
    String baseUrl, String searchText) async {
  try {
    Map<String, String> body;

    body = {'search': searchText};

    print('fetchSearchIbadahModel Url ' + baseUrl);

    print('fetchSearchIbadahModel Req ' + body.toString());

    final response = await http
        .post(baseUrl, body: body)
        .timeout(const Duration(seconds: 60));

    print('fetchSearchIbadahModel Res ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

SearchIbadahModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  SearchIbadahModel eventResponse = SearchIbadahModel.fromJson(data);
  return eventResponse;
}
