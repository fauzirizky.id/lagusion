import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/banner_model.dart';

Future<BannerModel> fetchBannerModel(String url) async {
  try {
    debugPrint('fetchBannerModel url : ' + url);

    final response = await http.get(url);

    debugPrint('fetchBannerModel Response : ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

BannerModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  BannerModel eventResponse = BannerModel.fromJson(data);
  return eventResponse;
}
