import 'package:music_mulai/model/audio_book_model.dart';
import 'package:music_mulai/model/banner_model.dart';
import 'package:music_mulai/model/feedback_model.dart';
import 'package:music_mulai/model/ibadah_model.dart';
import 'package:music_mulai/model/search_model/search_album_model.dart';
import 'package:music_mulai/model/search_model/search_audio_book_model.dart';
import 'package:music_mulai/model/search_model/search_ibadah_model.dart';
import 'package:music_mulai/model/song_service/song_list_model.dart';
import 'package:music_mulai/model/song_service/song_trending_model.dart';
import 'package:music_mulai/service/audio_book_service.dart';
import 'package:music_mulai/service/banner_service.dart';
import 'package:music_mulai/service/feedback_service.dart';
import 'package:music_mulai/service/ibadah_service.dart';
import 'package:music_mulai/service/search_service/search_album_service.dart';
import 'package:music_mulai/service/search_service/search_audio_book_service.dart';
import 'package:music_mulai/service/search_service/search_ibadah_service.dart';
import 'package:music_mulai/service/song_service/song_list_service.dart';
import 'package:music_mulai/service/song_service/song_trending_service.dart';

class ApiRepository {
  static const String BASE_URL                      = 'http://lagu-sion.demibangsa.com';

  static const String API_SONG_LIST                 = '$BASE_URL/api/song';
  static const String API_SONG_TRENDING             = '$BASE_URL/api/song/trending';
  static const String API_BANNER                    = '$BASE_URL/api/banner';
  static const String API_ALBUM_SEARCH              = '$BASE_URL/api/album/search';
  static const String API_AUDIO_BOOK                = '$BASE_URL/api/audio-book';
  static const String API_FEEDBACK                  = '$BASE_URL/api/feedback';
  static const String API_IBADAH                    = '$BASE_URL/api/ibadah/';
  static const String API_AUDIO_BOOK_SEARCH         = '$BASE_URL/api/audio-book/search';
  static const String API_IBADAH_SEARCH             = '$BASE_URL/api/ibadah/search';

  Future<SearchAlbumModel> fetchRepoSearchAlbumModel(String albumName) => fetchSearchAlbumModel(API_ALBUM_SEARCH, albumName);
  Future<SearchAudioBookModel> fetchRepoSearchAudioBookModel(String searchText) => fetchSearchAudioBookModel(API_AUDIO_BOOK_SEARCH, searchText);
  Future<SearchIbadahModel> fetchRepoSearchIbadahModel(String searchText) => fetchSearchIbadahModel(API_IBADAH_SEARCH, searchText);
  
  Future<SongListModel> fetchRepoSongListModel() => fetchSongListModel(API_SONG_LIST);
  Future<SongTrendingModel> fetchRepoSongTrendingModel() => fetchSongTrendingModel(API_SONG_TRENDING);
  
  Future<AudioBookModel> fetchRepoAudioBookModel() => fetchAudioBookModel(API_AUDIO_BOOK);
  Future<BannerModel> fetchRepoBannerModel() => fetchBannerModel(API_BANNER);
  Future<FeedbackModel> fetchRepoFeedbackModel(Map bodyValue) => fetchFeedbackModel(API_FEEDBACK, bodyValue);
  Future<IbadahModel> fetchRepoIbadahModel() => fetchIbadahModel(API_IBADAH);

}