import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:music_mulai/model/feedback_model.dart';

Future<FeedbackModel> fetchFeedbackModel(
    String baseUrl, Map bodyValue) async {
  try {
    print('fetchFeedbackModel Url ' + baseUrl);

    print('fetchFeedbackModel Req ' + bodyValue.toString());

    final response = await http
        .post(baseUrl, body: bodyValue)
        .timeout(const Duration(seconds: 60));

    print('fetchFeedbackModel Res ' + response.body);

    return compute(parsePosts, response.body);
  } catch (_) {
    return null;
  }
}

FeedbackModel parsePosts(String responseBody) {
  final parsed = json.decode(responseBody);
  var data = Map<String, dynamic>.from(parsed);
  FeedbackModel eventResponse = FeedbackModel.fromJson(data);
  return eventResponse;
}
