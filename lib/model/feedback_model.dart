// To parse this JSON data, do
//
//     final feedbackModel = feedbackModelFromJson(jsonString);

import 'dart:convert';

FeedbackModel feedbackModelFromJson(String str) => FeedbackModel.fromJson(json.decode(str));

String feedbackModelToJson(FeedbackModel data) => json.encode(data.toJson());

class FeedbackModel {
    FeedbackModel({
        this.status,
        this.message,
        this.feedback,
    });

    String status;
    String message;
    Feedback feedback;

    factory FeedbackModel.fromJson(Map<String, dynamic> json) => FeedbackModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        feedback: json["feedback"] == null ? null : Feedback.fromJson(json["feedback"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "feedback": feedback == null ? null : feedback.toJson(),
    };
}

class Feedback {
    Feedback({
        this.image,
        this.from,
        this.body,
        this.systemLog,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String image;
    String from;
    String body;
    String systemLog;
    String updatedAt;
    String createdAt;
    int id;

    factory Feedback.fromJson(Map<String, dynamic> json) => Feedback(
        image: json["image"] == null ? null : json["image"],
        from: json["from"] == null ? null : json["from"],
        body: json["body"] == null ? null : json["body"],
        systemLog: json["system_log"] == null ? null : json["system_log"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "image": image == null ? null : image,
        "from": from == null ? null : from,
        "body": body == null ? null : body,
        "system_log": systemLog == null ? null : systemLog,
        "updated_at": updatedAt == null ? null : updatedAt,
        "created_at": createdAt == null ? null : createdAt,
        "id": id == null ? null : id,
    };
}
