// To parse this JSON data, do
//
//     final searchAudioBookModel = searchAudioBookModelFromJson(jsonString);

import 'dart:convert';

SearchAudioBookModel searchAudioBookModelFromJson(String str) => SearchAudioBookModel.fromJson(json.decode(str));

String searchAudioBookModelToJson(SearchAudioBookModel data) => json.encode(data.toJson());

class SearchAudioBookModel {
    SearchAudioBookModel({
        this.status,
        this.message,
        this.audioBooks,
    });

    String status;
    String message;
    List<AudioBook> audioBooks;

    factory SearchAudioBookModel.fromJson(Map<String, dynamic> json) => SearchAudioBookModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        audioBooks: json["audio_books"] == null ? null : List<AudioBook>.from(json["audio_books"].map((x) => AudioBook.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "audio_books": audioBooks == null ? null : List<dynamic>.from(audioBooks.map((x) => x.toJson())),
    };
}

class AudioBook {
    AudioBook({
        this.id,
        this.thumbnail,
        this.audioBookName,
        this.createdAt,
        this.updatedAt,
        this.parts,
    });

    int id;
    String thumbnail;
    String audioBookName;
    String createdAt;
    String updatedAt;
    List<Part> parts;

    factory AudioBook.fromJson(Map<String, dynamic> json) => AudioBook(
        id: json["id"] == null ? null : json["id"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        audioBookName: json["audio_book_name"] == null ? null : json["audio_book_name"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        parts: json["parts"] == null ? null : List<Part>.from(json["parts"].map((x) => Part.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "audio_book_name": audioBookName == null ? null : audioBookName,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "parts": parts == null ? null : List<dynamic>.from(parts.map((x) => x.toJson())),
    };
}

class Part {
    Part({
        this.id,
        this.audioBookId,
        this.title,
        this.duration,
        this.lyrics,
        this.music,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    int audioBookId;
    String title;
    String duration;
    String lyrics;
    String music;
    dynamic createdAt;
    dynamic updatedAt;

    factory Part.fromJson(Map<String, dynamic> json) => Part(
        id: json["id"] == null ? null : json["id"],
        audioBookId: json["audio_book_id"] == null ? null : json["audio_book_id"],
        title: json["title"] == null ? null : json["title"],
        duration: json["duration"] == null ? null : json["duration"],
        lyrics: json["lyrics"] == null ? null : json["lyrics"],
        music: json["music"] == null ? null : json["music"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "audio_book_id": audioBookId == null ? null : audioBookId,
        "title": title == null ? null : title,
        "duration": duration == null ? null : duration,
        "lyrics": lyrics == null ? null : lyrics,
        "music": music == null ? null : music,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
