// To parse this JSON data, do
//
//     final audioBookModel = audioBookModelFromJson(jsonString);

import 'dart:convert';

AudioBookModel audioBookModelFromJson(String str) => AudioBookModel.fromJson(json.decode(str));

String audioBookModelToJson(AudioBookModel data) => json.encode(data.toJson());

class AudioBookModel {
    AudioBookModel({
        this.status,
        this.message,
        this.audioBooks,
    });

    String status;
    String message;
    List<AudioBook> audioBooks;

    factory AudioBookModel.fromJson(Map<String, dynamic> json) => AudioBookModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"] == null ? null : json["message"],
        audioBooks: json["audio_books"] == null ? null : List<AudioBook>.from(json["audio_books"].map((x) => AudioBook.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message == null ? null : message,
        "audio_books": audioBooks == null ? null : List<dynamic>.from(audioBooks.map((x) => x.toJson())),
    };
}

class AudioBook {
    AudioBook({
        this.id,
        this.thumbnail,
        this.audioBookName,
        this.createdAt,
        this.updatedAt,
        this.links,
        this.parts,
    });

    int id;
    String thumbnail;
    String audioBookName;
    String createdAt;
    String updatedAt;
    List<Link> links;
    List<Part> parts;

    factory AudioBook.fromJson(Map<String, dynamic> json) => AudioBook(
        id: json["id"] == null ? null : json["id"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        audioBookName: json["audio_book_name"] == null ? null : json["audio_book_name"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        links: json["links"] == null ? null : List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
        parts: json["parts"] == null ? null : List<Part>.from(json["parts"].map((x) => Part.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "audio_book_name": audioBookName == null ? null : audioBookName,
        "created_at": createdAt == null ? null : createdAt,
        "updated_at": updatedAt == null ? null : updatedAt,
        "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
        "parts": parts == null ? null : List<dynamic>.from(parts.map((x) => x.toJson())),
    };
}

class Link {
    Link({
        this.id,
        this.music,
    });

    int id;
    String music;

    factory Link.fromJson(Map<String, dynamic> json) => Link(
        id: json["id"] == null ? null : json["id"],
        music: json["music"] == null ? null : json["music"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "music": music == null ? null : music,
    };
}

class Part {
    Part({
        this.id,
        this.audioBookId,
        this.title,
        this.duration,
        this.lyrics,
        this.music,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    int audioBookId;
    String title;
    String duration;
    String lyrics;
    String music;
    dynamic createdAt;
    dynamic updatedAt;

    factory Part.fromJson(Map<String, dynamic> json) => Part(
        id: json["id"] == null ? null : json["id"],
        audioBookId: json["audio_book_id"] == null ? null : json["audio_book_id"],
        title: json["title"] == null ? null : json["title"],
        duration: json["duration"] == null ? null : json["duration"],
        lyrics: json["lyrics"] == null ? null : json["lyrics"],
        music: json["music"] == null ? null : json["music"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "audio_book_id": audioBookId == null ? null : audioBookId,
        "title": title == null ? null : title,
        "duration": duration == null ? null : duration,
        "lyrics": lyrics == null ? null : lyrics,
        "music": music == null ? null : music,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
