part of 'splash_bloc.dart';

abstract class SplashState extends Equatable {
  const SplashState();

  @override
  List<Object> get props => [];
}

class SplashInitial extends SplashState {}

class SplashGetDataSuccess extends SplashState {}

class HitApiFailed extends SplashState {
  final String errorMessage;
  final bool isConnected;

  HitApiFailed(this.errorMessage, this.isConnected);
}

