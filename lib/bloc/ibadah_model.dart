class AudioIbadahModel {
  int id;
  String thumbnail;
  String ibadahBookName;
  String createdAt;
  String updatedAt;
  List<Links> links;
  List<Parts> parts;

  AudioIbadahModel(
      {this.id,
        this.thumbnail,
        this.ibadahBookName,
        this.createdAt,
        this.updatedAt,
        this.links,
        this.parts});

  AudioIbadahModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    thumbnail = json['thumbnail'];
    ibadahBookName = json['ibadah_name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['links'] != null) {
      links = new List<Links>();
      json['links'].forEach((v) {
        links.add(new Links.fromJson(v));
      });
    }
    if (json['parts'] != null) {
      parts = new List<Parts>();
      json['parts'].forEach((v) {
        parts.add(new Parts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['thumbnail'] = this.thumbnail;
    data['ibadah_name'] = this.ibadahBookName;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.links != null) {
      data['links'] = this.links.map((v) => v.toJson()).toList();
    }
    if (this.parts != null) {
      data['parts'] = this.parts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Links {
  int id;
  String music;

  Links({this.id, this.music});

  Links.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    music = json['music'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['music'] = this.music;
    return data;
  }
}

class Parts {
  int id;
  int audioBookId;
  String title;
  String duration;
  String lyrics;
  String music;
  Null createdAt;
  Null updatedAt;

  Parts(
      {this.id,
        this.audioBookId,
        this.title,
        this.duration,
        this.lyrics,
        this.music,
        this.createdAt,
        this.updatedAt});

  Parts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    audioBookId = json['ibadah_id'];
    title = json['title'];
    duration = json['duration'];
    lyrics = json['lyrics'];
    music = json['music'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['audio_book_id'] = this.audioBookId;
    data['title'] = this.title;
    data['duration'] = this.duration;
    data['lyrics'] = this.lyrics;
    data['music'] = this.music;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}