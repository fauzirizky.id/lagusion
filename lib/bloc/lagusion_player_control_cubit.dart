import 'package:audioplayers/audioplayers.dart';
import 'package:bloc/bloc.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/bloc/lagusion_player_state.dart';
import 'package:music_mulai/global_audio_player.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum AudioPlayerDetailType {
  HOME,
  BOOK,
  PRAY,
  DOWNLOADED,
}

class LagusionPlayerControlCubit extends Cubit<LagusionPlayerState> {
  LagusionPlayerControlCubit() : super(Idle()) {
    LagusionAudioPlayer.instance.onPlayerStateChanged.listen((event) {
      _notifyDataChange();
    });
  }


  List<dynamic> _listMusicDynamic;
  int currentIndexPlay = -1;
  AudioPlayerDetailType _type;
  String _thumb;

  void playAudio(int index, List<dynamic> currentPlaylist,
      AudioPlayerDetailType type, {String thumb}) async {
    _thumb = thumb;
    _updateCurrentPlaylist(currentPlaylist);
    _setCurrentPlay(currentPlaylist[index]['id']);
    currentIndexPlay = index;
    _type = type;
    _notifyDataChange();
  }

  void stopAudio() {
    LagusionAudioPlayer.instance.stop();
    _notifyDataChange();
  }

  void nextAudio() {
    currentIndexPlay++;
    LagusionAudioPlayer.instance.play(_listMusicDynamic[currentIndexPlay]['music']);
    _notifyDataChange();
  }

  void previousAudio() {
    if (currentIndexPlay >= 0) {
      currentIndexPlay--;
      LagusionAudioPlayer.instance.play(_listMusicDynamic[currentIndexPlay]['music']);
      _notifyDataChange();
    }
  }

  void _setCurrentPlay(int idMusic) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('music_play', idMusic);
  }

  void _updateCurrentPlaylist(List<dynamic> currentPlaylist) {
    _listMusicDynamic = currentPlaylist;
  }

  void _notifyDataChange() {
    if (currentIndexPlay >= 0) {
      if(LagusionAudioPlayer.instance.state != AudioPlayerState.STOPPED){
        final currentPlay = _listMusicDynamic[currentIndexPlay];
        emit(LagusionIsPlaying(
            LagusionAudioPlayer.instance.state, currentPlay, _type, _listMusicDynamic, currentIndexPlay, _thumb));
        _setCurrentPlay(currentPlay['id']);
      }else {
        emit(Idle());
      }
    }
  }
}
