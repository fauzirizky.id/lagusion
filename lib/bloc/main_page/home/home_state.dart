part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HitApiFailed extends HomeState {
  final String errorMessage;

  HitApiFailed(this.errorMessage);
}

class HomeGetBannerSuccess extends HomeState {
  final BannerModel bannerModel;

  HomeGetBannerSuccess({this.bannerModel});
}

class HomeGetSongTrendingSuccess extends HomeState {
  final SongTrendingModel songTrendingModel;

  HomeGetSongTrendingSuccess({this.songTrendingModel});
}
