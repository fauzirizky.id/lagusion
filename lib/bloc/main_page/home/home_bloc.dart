import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:music_mulai/model/banner_model.dart';
import 'package:music_mulai/model/song_service/song_trending_model.dart';
import 'package:music_mulai/service/api_repository.dart';

import 'package:music_mulai/model/search_model/search_album_model.dart'
    as SearchAlbumModel;
import 'package:music_mulai/model/song_service/song_list_model.dart'
    as SongListModel;
import 'package:music_mulai/util/shared_data_get.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    final apiRepository = ApiRepository();
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    if (event is HomeRefresh) {
      try {
        final banner = await apiRepository.fetchRepoBannerModel();
        if (banner.status == 'OK') {
          try {
            final trendingSong =
                await apiRepository.fetchRepoSongTrendingModel();

            if (trendingSong.status == 'OK') {
              yield HomeGetBannerSuccess(bannerModel: banner);
              yield HomeGetSongTrendingSuccess(songTrendingModel: trendingSong);
            } else {
              yield HitApiFailed('Gagal mendapatkan data');
            }
          } catch (e) {
            yield HitApiFailed('Gagal mendapatkan data');
          }
        }
      } catch (e) {
        yield HitApiFailed('Gagal mendapatkan data');
      }
    }
  }
}
