part of 'songs_bloc.dart';

abstract class SongsState extends Equatable {
  const SongsState();

  @override
  List<Object> get props => [];
}

class SongsInitial extends SongsState {}

class SongsLoading extends SongsState {}

class HitApiFailed extends SongsState {
  final String errorMessage;

  HitApiFailed(this.errorMessage);
}

class SongsGetAlbumsSuccess extends SongsState {
  final List<SearchAlbumModel.Album> albums;

  SongsGetAlbumsSuccess({this.albums});
}

class SongsGetListSongSuccess extends SongsState {
  final List<SongListModel.Song> listSong;
  final List<SongListModel.Link> listSongLink;

  SongsGetListSongSuccess({this.listSong, this.listSongLink});
}
