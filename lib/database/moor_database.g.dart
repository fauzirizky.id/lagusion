// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class Song extends DataClass implements Insertable<Song> {
  final int id;
  final int idMusic;
  final String data;
  final int musicVersion;
  final DateTime created_at;
  Song(
      {@required this.id,
      @required this.idMusic,
      @required this.data,
      @required this.musicVersion,
      @required this.created_at});
  factory Song.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Song(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      idMusic:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}id_music']),
      data: stringType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      musicVersion: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}music_version']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory Song.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Song(
      id: serializer.fromJson<int>(json['id']),
      idMusic: serializer.fromJson<int>(json['idMusic']),
      data: serializer.fromJson<String>(json['data']),
      musicVersion: serializer.fromJson<int>(json['musicVersion']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'idMusic': serializer.toJson<int>(idMusic),
      'data': serializer.toJson<String>(data),
      'musicVersion': serializer.toJson<int>(musicVersion),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Song>>(bool nullToAbsent) {
    return SongsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      idMusic: idMusic == null && nullToAbsent
          ? const Value.absent()
          : Value(idMusic),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      musicVersion: musicVersion == null && nullToAbsent
          ? const Value.absent()
          : Value(musicVersion),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  Song copyWith(
          {int id,
          int idMusic,
          String data,
          int musicVersion,
          DateTime created_at}) =>
      Song(
        id: id ?? this.id,
        idMusic: idMusic ?? this.idMusic,
        data: data ?? this.data,
        musicVersion: musicVersion ?? this.musicVersion,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('Song(')
          ..write('id: $id, ')
          ..write('idMusic: $idMusic, ')
          ..write('data: $data, ')
          ..write('musicVersion: $musicVersion, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idMusic.hashCode,
          $mrjc(data.hashCode,
              $mrjc(musicVersion.hashCode, created_at.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Song &&
          other.id == id &&
          other.idMusic == idMusic &&
          other.data == data &&
          other.musicVersion == musicVersion &&
          other.created_at == created_at);
}

class SongsCompanion extends UpdateCompanion<Song> {
  final Value<int> id;
  final Value<int> idMusic;
  final Value<String> data;
  final Value<int> musicVersion;
  final Value<DateTime> created_at;
  const SongsCompanion({
    this.id = const Value.absent(),
    this.idMusic = const Value.absent(),
    this.data = const Value.absent(),
    this.musicVersion = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  SongsCompanion copyWith(
      {Value<int> id,
      Value<int> idMusic,
      Value<String> data,
      Value<int> musicVersion,
      Value<DateTime> created_at}) {
    return SongsCompanion(
      id: id ?? this.id,
      idMusic: idMusic ?? this.idMusic,
      data: data ?? this.data,
      musicVersion: musicVersion ?? this.musicVersion,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $SongsTable extends Songs with TableInfo<$SongsTable, Song> {
  final GeneratedDatabase _db;
  final String _alias;
  $SongsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _idMusicMeta = const VerificationMeta('idMusic');
  GeneratedIntColumn _idMusic;
  @override
  GeneratedIntColumn get idMusic => _idMusic ??= _constructIdMusic();
  GeneratedIntColumn _constructIdMusic() {
    return GeneratedIntColumn(
      'id_music',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedTextColumn _data;
  @override
  GeneratedTextColumn get data => _data ??= _constructData();
  GeneratedTextColumn _constructData() {
    return GeneratedTextColumn(
      'data',
      $tableName,
      false,
    );
  }

  final VerificationMeta _musicVersionMeta =
      const VerificationMeta('musicVersion');
  GeneratedIntColumn _musicVersion;
  @override
  GeneratedIntColumn get musicVersion =>
      _musicVersion ??= _constructMusicVersion();
  GeneratedIntColumn _constructMusicVersion() {
    return GeneratedIntColumn(
      'music_version',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, idMusic, data, musicVersion, created_at];
  @override
  $SongsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'songs';
  @override
  final String actualTableName = 'songs';
  @override
  VerificationContext validateIntegrity(SongsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.idMusic.present) {
      context.handle(_idMusicMeta,
          idMusic.isAcceptableValue(d.idMusic.value, _idMusicMeta));
    } else if (idMusic.isRequired && isInserting) {
      context.missing(_idMusicMeta);
    }
    if (d.data.present) {
      context.handle(
          _dataMeta, data.isAcceptableValue(d.data.value, _dataMeta));
    } else if (data.isRequired && isInserting) {
      context.missing(_dataMeta);
    }
    if (d.musicVersion.present) {
      context.handle(
          _musicVersionMeta,
          musicVersion.isAcceptableValue(
              d.musicVersion.value, _musicVersionMeta));
    } else if (musicVersion.isRequired && isInserting) {
      context.missing(_musicVersionMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Song map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Song.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(SongsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.idMusic.present) {
      map['id_music'] = Variable<int, IntType>(d.idMusic.value);
    }
    if (d.data.present) {
      map['data'] = Variable<String, StringType>(d.data.value);
    }
    if (d.musicVersion.present) {
      map['music_version'] = Variable<int, IntType>(d.musicVersion.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $SongsTable createAlias(String alias) {
    return $SongsTable(_db, alias);
  }
}

class Playlist extends DataClass implements Insertable<Playlist> {
  final int id;
  final int playlist_id;
  final int music_id;
  final DateTime created_at;
  Playlist(
      {@required this.id,
      @required this.playlist_id,
      @required this.music_id,
      @required this.created_at});
  factory Playlist.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Playlist(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      playlist_id: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}playlist_id']),
      music_id:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}music_id']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory Playlist.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Playlist(
      id: serializer.fromJson<int>(json['id']),
      playlist_id: serializer.fromJson<int>(json['playlist_id']),
      music_id: serializer.fromJson<int>(json['music_id']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'playlist_id': serializer.toJson<int>(playlist_id),
      'music_id': serializer.toJson<int>(music_id),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Playlist>>(bool nullToAbsent) {
    return PlaylistsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      playlist_id: playlist_id == null && nullToAbsent
          ? const Value.absent()
          : Value(playlist_id),
      music_id: music_id == null && nullToAbsent
          ? const Value.absent()
          : Value(music_id),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  Playlist copyWith(
          {int id, int playlist_id, int music_id, DateTime created_at}) =>
      Playlist(
        id: id ?? this.id,
        playlist_id: playlist_id ?? this.playlist_id,
        music_id: music_id ?? this.music_id,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('Playlist(')
          ..write('id: $id, ')
          ..write('playlist_id: $playlist_id, ')
          ..write('music_id: $music_id, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(playlist_id.hashCode,
          $mrjc(music_id.hashCode, created_at.hashCode))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Playlist &&
          other.id == id &&
          other.playlist_id == playlist_id &&
          other.music_id == music_id &&
          other.created_at == created_at);
}

class PlaylistsCompanion extends UpdateCompanion<Playlist> {
  final Value<int> id;
  final Value<int> playlist_id;
  final Value<int> music_id;
  final Value<DateTime> created_at;
  const PlaylistsCompanion({
    this.id = const Value.absent(),
    this.playlist_id = const Value.absent(),
    this.music_id = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  PlaylistsCompanion copyWith(
      {Value<int> id,
      Value<int> playlist_id,
      Value<int> music_id,
      Value<DateTime> created_at}) {
    return PlaylistsCompanion(
      id: id ?? this.id,
      playlist_id: playlist_id ?? this.playlist_id,
      music_id: music_id ?? this.music_id,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $PlaylistsTable extends Playlists
    with TableInfo<$PlaylistsTable, Playlist> {
  final GeneratedDatabase _db;
  final String _alias;
  $PlaylistsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _playlist_idMeta =
      const VerificationMeta('playlist_id');
  GeneratedIntColumn _playlist_id;
  @override
  GeneratedIntColumn get playlist_id => _playlist_id ??= _constructPlaylistId();
  GeneratedIntColumn _constructPlaylistId() {
    return GeneratedIntColumn(
      'playlist_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _music_idMeta = const VerificationMeta('music_id');
  GeneratedIntColumn _music_id;
  @override
  GeneratedIntColumn get music_id => _music_id ??= _constructMusicId();
  GeneratedIntColumn _constructMusicId() {
    return GeneratedIntColumn(
      'music_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, playlist_id, music_id, created_at];
  @override
  $PlaylistsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'playlists';
  @override
  final String actualTableName = 'playlists';
  @override
  VerificationContext validateIntegrity(PlaylistsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.playlist_id.present) {
      context.handle(_playlist_idMeta,
          playlist_id.isAcceptableValue(d.playlist_id.value, _playlist_idMeta));
    } else if (playlist_id.isRequired && isInserting) {
      context.missing(_playlist_idMeta);
    }
    if (d.music_id.present) {
      context.handle(_music_idMeta,
          music_id.isAcceptableValue(d.music_id.value, _music_idMeta));
    } else if (music_id.isRequired && isInserting) {
      context.missing(_music_idMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Playlist map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Playlist.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(PlaylistsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.playlist_id.present) {
      map['playlist_id'] = Variable<int, IntType>(d.playlist_id.value);
    }
    if (d.music_id.present) {
      map['music_id'] = Variable<int, IntType>(d.music_id.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $PlaylistsTable createAlias(String alias) {
    return $PlaylistsTable(_db, alias);
  }
}

class JudulPlaylist extends DataClass implements Insertable<JudulPlaylist> {
  final int id;
  final String judul_playlist;
  final DateTime created_at;
  JudulPlaylist(
      {@required this.id,
      @required this.judul_playlist,
      @required this.created_at});
  factory JudulPlaylist.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return JudulPlaylist(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      judul_playlist: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}judul_playlist']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory JudulPlaylist.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return JudulPlaylist(
      id: serializer.fromJson<int>(json['id']),
      judul_playlist: serializer.fromJson<String>(json['judul_playlist']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'judul_playlist': serializer.toJson<String>(judul_playlist),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<JudulPlaylist>>(
      bool nullToAbsent) {
    return JudulPlaylistsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      judul_playlist: judul_playlist == null && nullToAbsent
          ? const Value.absent()
          : Value(judul_playlist),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  JudulPlaylist copyWith(
          {int id, String judul_playlist, DateTime created_at}) =>
      JudulPlaylist(
        id: id ?? this.id,
        judul_playlist: judul_playlist ?? this.judul_playlist,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('JudulPlaylist(')
          ..write('id: $id, ')
          ..write('judul_playlist: $judul_playlist, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf(
      $mrjc(id.hashCode, $mrjc(judul_playlist.hashCode, created_at.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is JudulPlaylist &&
          other.id == id &&
          other.judul_playlist == judul_playlist &&
          other.created_at == created_at);
}

class JudulPlaylistsCompanion extends UpdateCompanion<JudulPlaylist> {
  final Value<int> id;
  final Value<String> judul_playlist;
  final Value<DateTime> created_at;
  const JudulPlaylistsCompanion({
    this.id = const Value.absent(),
    this.judul_playlist = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  JudulPlaylistsCompanion copyWith(
      {Value<int> id,
      Value<String> judul_playlist,
      Value<DateTime> created_at}) {
    return JudulPlaylistsCompanion(
      id: id ?? this.id,
      judul_playlist: judul_playlist ?? this.judul_playlist,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $JudulPlaylistsTable extends JudulPlaylists
    with TableInfo<$JudulPlaylistsTable, JudulPlaylist> {
  final GeneratedDatabase _db;
  final String _alias;
  $JudulPlaylistsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _judul_playlistMeta =
      const VerificationMeta('judul_playlist');
  GeneratedTextColumn _judul_playlist;
  @override
  GeneratedTextColumn get judul_playlist =>
      _judul_playlist ??= _constructJudulPlaylist();
  GeneratedTextColumn _constructJudulPlaylist() {
    return GeneratedTextColumn('judul_playlist', $tableName, false,
        minTextLength: 1, maxTextLength: 250);
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, judul_playlist, created_at];
  @override
  $JudulPlaylistsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'judul_playlists';
  @override
  final String actualTableName = 'judul_playlists';
  @override
  VerificationContext validateIntegrity(JudulPlaylistsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.judul_playlist.present) {
      context.handle(
          _judul_playlistMeta,
          judul_playlist.isAcceptableValue(
              d.judul_playlist.value, _judul_playlistMeta));
    } else if (judul_playlist.isRequired && isInserting) {
      context.missing(_judul_playlistMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  JudulPlaylist map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return JudulPlaylist.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(JudulPlaylistsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.judul_playlist.present) {
      map['judul_playlist'] =
          Variable<String, StringType>(d.judul_playlist.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $JudulPlaylistsTable createAlias(String alias) {
    return $JudulPlaylistsTable(_db, alias);
  }
}

class Favorit extends DataClass implements Insertable<Favorit> {
  final int id;
  final int music_id;
  final DateTime created_at;
  Favorit(
      {@required this.id, @required this.music_id, @required this.created_at});
  factory Favorit.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Favorit(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      music_id:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}music_id']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory Favorit.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Favorit(
      id: serializer.fromJson<int>(json['id']),
      music_id: serializer.fromJson<int>(json['music_id']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'music_id': serializer.toJson<int>(music_id),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Favorit>>(bool nullToAbsent) {
    return FavoritsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      music_id: music_id == null && nullToAbsent
          ? const Value.absent()
          : Value(music_id),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  Favorit copyWith({int id, int music_id, DateTime created_at}) => Favorit(
        id: id ?? this.id,
        music_id: music_id ?? this.music_id,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('Favorit(')
          ..write('id: $id, ')
          ..write('music_id: $music_id, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(music_id.hashCode, created_at.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Favorit &&
          other.id == id &&
          other.music_id == music_id &&
          other.created_at == created_at);
}

class FavoritsCompanion extends UpdateCompanion<Favorit> {
  final Value<int> id;
  final Value<int> music_id;
  final Value<DateTime> created_at;
  const FavoritsCompanion({
    this.id = const Value.absent(),
    this.music_id = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  FavoritsCompanion copyWith(
      {Value<int> id, Value<int> music_id, Value<DateTime> created_at}) {
    return FavoritsCompanion(
      id: id ?? this.id,
      music_id: music_id ?? this.music_id,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $FavoritsTable extends Favorits with TableInfo<$FavoritsTable, Favorit> {
  final GeneratedDatabase _db;
  final String _alias;
  $FavoritsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _music_idMeta = const VerificationMeta('music_id');
  GeneratedIntColumn _music_id;
  @override
  GeneratedIntColumn get music_id => _music_id ??= _constructMusicId();
  GeneratedIntColumn _constructMusicId() {
    return GeneratedIntColumn(
      'music_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, music_id, created_at];
  @override
  $FavoritsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'favorits';
  @override
  final String actualTableName = 'favorits';
  @override
  VerificationContext validateIntegrity(FavoritsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.music_id.present) {
      context.handle(_music_idMeta,
          music_id.isAcceptableValue(d.music_id.value, _music_idMeta));
    } else if (music_id.isRequired && isInserting) {
      context.missing(_music_idMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Favorit map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Favorit.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(FavoritsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.music_id.present) {
      map['music_id'] = Variable<int, IntType>(d.music_id.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $FavoritsTable createAlias(String alias) {
    return $FavoritsTable(_db, alias);
  }
}

class Inbox extends DataClass implements Insertable<Inbox> {
  final int id;
  final int inbox_id;
  final DateTime created_at;
  Inbox(
      {@required this.id, @required this.inbox_id, @required this.created_at});
  factory Inbox.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Inbox(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      inbox_id:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}inbox_id']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory Inbox.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Inbox(
      id: serializer.fromJson<int>(json['id']),
      inbox_id: serializer.fromJson<int>(json['inbox_id']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'inbox_id': serializer.toJson<int>(inbox_id),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Inbox>>(bool nullToAbsent) {
    return InboxsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      inbox_id: inbox_id == null && nullToAbsent
          ? const Value.absent()
          : Value(inbox_id),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  Inbox copyWith({int id, int inbox_id, DateTime created_at}) => Inbox(
        id: id ?? this.id,
        inbox_id: inbox_id ?? this.inbox_id,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('Inbox(')
          ..write('id: $id, ')
          ..write('inbox_id: $inbox_id, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(inbox_id.hashCode, created_at.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Inbox &&
          other.id == id &&
          other.inbox_id == inbox_id &&
          other.created_at == created_at);
}

class InboxsCompanion extends UpdateCompanion<Inbox> {
  final Value<int> id;
  final Value<int> inbox_id;
  final Value<DateTime> created_at;
  const InboxsCompanion({
    this.id = const Value.absent(),
    this.inbox_id = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  InboxsCompanion copyWith(
      {Value<int> id, Value<int> inbox_id, Value<DateTime> created_at}) {
    return InboxsCompanion(
      id: id ?? this.id,
      inbox_id: inbox_id ?? this.inbox_id,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $InboxsTable extends Inboxs with TableInfo<$InboxsTable, Inbox> {
  final GeneratedDatabase _db;
  final String _alias;
  $InboxsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _inbox_idMeta = const VerificationMeta('inbox_id');
  GeneratedIntColumn _inbox_id;
  @override
  GeneratedIntColumn get inbox_id => _inbox_id ??= _constructInboxId();
  GeneratedIntColumn _constructInboxId() {
    return GeneratedIntColumn(
      'inbox_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, inbox_id, created_at];
  @override
  $InboxsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'inboxs';
  @override
  final String actualTableName = 'inboxs';
  @override
  VerificationContext validateIntegrity(InboxsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.inbox_id.present) {
      context.handle(_inbox_idMeta,
          inbox_id.isAcceptableValue(d.inbox_id.value, _inbox_idMeta));
    } else if (inbox_id.isRequired && isInserting) {
      context.missing(_inbox_idMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Inbox map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Inbox.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(InboxsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.inbox_id.present) {
      map['inbox_id'] = Variable<int, IntType>(d.inbox_id.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $InboxsTable createAlias(String alias) {
    return $InboxsTable(_db, alias);
  }
}

class RecentSong extends DataClass implements Insertable<RecentSong> {
  final int id;
  final int music_id;
  final DateTime created_at;
  RecentSong(
      {@required this.id, @required this.music_id, @required this.created_at});
  factory RecentSong.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return RecentSong(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      music_id:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}music_id']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory RecentSong.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return RecentSong(
      id: serializer.fromJson<int>(json['id']),
      music_id: serializer.fromJson<int>(json['music_id']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'music_id': serializer.toJson<int>(music_id),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<RecentSong>>(bool nullToAbsent) {
    return RecentSongsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      music_id: music_id == null && nullToAbsent
          ? const Value.absent()
          : Value(music_id),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  RecentSong copyWith({int id, int music_id, DateTime created_at}) =>
      RecentSong(
        id: id ?? this.id,
        music_id: music_id ?? this.music_id,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('RecentSong(')
          ..write('id: $id, ')
          ..write('music_id: $music_id, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(music_id.hashCode, created_at.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is RecentSong &&
          other.id == id &&
          other.music_id == music_id &&
          other.created_at == created_at);
}

class RecentSongsCompanion extends UpdateCompanion<RecentSong> {
  final Value<int> id;
  final Value<int> music_id;
  final Value<DateTime> created_at;
  const RecentSongsCompanion({
    this.id = const Value.absent(),
    this.music_id = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  RecentSongsCompanion copyWith(
      {Value<int> id, Value<int> music_id, Value<DateTime> created_at}) {
    return RecentSongsCompanion(
      id: id ?? this.id,
      music_id: music_id ?? this.music_id,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $RecentSongsTable extends RecentSongs
    with TableInfo<$RecentSongsTable, RecentSong> {
  final GeneratedDatabase _db;
  final String _alias;
  $RecentSongsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _music_idMeta = const VerificationMeta('music_id');
  GeneratedIntColumn _music_id;
  @override
  GeneratedIntColumn get music_id => _music_id ??= _constructMusicId();
  GeneratedIntColumn _constructMusicId() {
    return GeneratedIntColumn(
      'music_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, music_id, created_at];
  @override
  $RecentSongsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'recent_songs';
  @override
  final String actualTableName = 'recent_songs';
  @override
  VerificationContext validateIntegrity(RecentSongsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.music_id.present) {
      context.handle(_music_idMeta,
          music_id.isAcceptableValue(d.music_id.value, _music_idMeta));
    } else if (music_id.isRequired && isInserting) {
      context.missing(_music_idMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  RecentSong map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return RecentSong.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(RecentSongsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.music_id.present) {
      map['music_id'] = Variable<int, IntType>(d.music_id.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $RecentSongsTable createAlias(String alias) {
    return $RecentSongsTable(_db, alias);
  }
}

class RecentBook extends DataClass implements Insertable<RecentBook> {
  final int id;
  final int book_id;
  final DateTime created_at;
  RecentBook(
      {@required this.id, @required this.book_id, @required this.created_at});
  factory RecentBook.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return RecentBook(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      book_id:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}book_id']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory RecentBook.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return RecentBook(
      id: serializer.fromJson<int>(json['id']),
      book_id: serializer.fromJson<int>(json['book_id']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'book_id': serializer.toJson<int>(book_id),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<RecentBook>>(bool nullToAbsent) {
    return RecentBooksCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      book_id: book_id == null && nullToAbsent
          ? const Value.absent()
          : Value(book_id),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  RecentBook copyWith({int id, int book_id, DateTime created_at}) => RecentBook(
        id: id ?? this.id,
        book_id: book_id ?? this.book_id,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('RecentBook(')
          ..write('id: $id, ')
          ..write('book_id: $book_id, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(book_id.hashCode, created_at.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is RecentBook &&
          other.id == id &&
          other.book_id == book_id &&
          other.created_at == created_at);
}

class RecentBooksCompanion extends UpdateCompanion<RecentBook> {
  final Value<int> id;
  final Value<int> book_id;
  final Value<DateTime> created_at;
  const RecentBooksCompanion({
    this.id = const Value.absent(),
    this.book_id = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  RecentBooksCompanion copyWith(
      {Value<int> id, Value<int> book_id, Value<DateTime> created_at}) {
    return RecentBooksCompanion(
      id: id ?? this.id,
      book_id: book_id ?? this.book_id,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $RecentBooksTable extends RecentBooks
    with TableInfo<$RecentBooksTable, RecentBook> {
  final GeneratedDatabase _db;
  final String _alias;
  $RecentBooksTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _book_idMeta = const VerificationMeta('book_id');
  GeneratedIntColumn _book_id;
  @override
  GeneratedIntColumn get book_id => _book_id ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn(
      'book_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, book_id, created_at];
  @override
  $RecentBooksTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'recent_books';
  @override
  final String actualTableName = 'recent_books';
  @override
  VerificationContext validateIntegrity(RecentBooksCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.book_id.present) {
      context.handle(_book_idMeta,
          book_id.isAcceptableValue(d.book_id.value, _book_idMeta));
    } else if (book_id.isRequired && isInserting) {
      context.missing(_book_idMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  RecentBook map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return RecentBook.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(RecentBooksCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.book_id.present) {
      map['book_id'] = Variable<int, IntType>(d.book_id.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $RecentBooksTable createAlias(String alias) {
    return $RecentBooksTable(_db, alias);
  }
}

class Ibadah extends DataClass implements Insertable<Ibadah> {
  final int id;
  final int ibadahId;
  final String data;
  final DateTime created_at;
  Ibadah(
      {@required this.id,
      @required this.ibadahId,
      @required this.data,
      @required this.created_at});
  factory Ibadah.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Ibadah(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      ibadahId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}ibadah_id']),
      data: stringType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory Ibadah.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Ibadah(
      id: serializer.fromJson<int>(json['id']),
      ibadahId: serializer.fromJson<int>(json['ibadahId']),
      data: serializer.fromJson<String>(json['data']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'ibadahId': serializer.toJson<int>(ibadahId),
      'data': serializer.toJson<String>(data),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Ibadah>>(bool nullToAbsent) {
    return IbadahsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      ibadahId: ibadahId == null && nullToAbsent
          ? const Value.absent()
          : Value(ibadahId),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  Ibadah copyWith({int id, int ibadahId, String data, DateTime created_at}) =>
      Ibadah(
        id: id ?? this.id,
        ibadahId: ibadahId ?? this.ibadahId,
        data: data ?? this.data,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('Ibadah(')
          ..write('id: $id, ')
          ..write('ibadahId: $ibadahId, ')
          ..write('data: $data, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(ibadahId.hashCode, $mrjc(data.hashCode, created_at.hashCode))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Ibadah &&
          other.id == id &&
          other.ibadahId == ibadahId &&
          other.data == data &&
          other.created_at == created_at);
}

class IbadahsCompanion extends UpdateCompanion<Ibadah> {
  final Value<int> id;
  final Value<int> ibadahId;
  final Value<String> data;
  final Value<DateTime> created_at;
  const IbadahsCompanion({
    this.id = const Value.absent(),
    this.ibadahId = const Value.absent(),
    this.data = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  IbadahsCompanion copyWith(
      {Value<int> id,
      Value<int> ibadahId,
      Value<String> data,
      Value<DateTime> created_at}) {
    return IbadahsCompanion(
      id: id ?? this.id,
      ibadahId: ibadahId ?? this.ibadahId,
      data: data ?? this.data,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $IbadahsTable extends Ibadahs with TableInfo<$IbadahsTable, Ibadah> {
  final GeneratedDatabase _db;
  final String _alias;
  $IbadahsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _ibadahIdMeta = const VerificationMeta('ibadahId');
  GeneratedIntColumn _ibadahId;
  @override
  GeneratedIntColumn get ibadahId => _ibadahId ??= _constructIbadahId();
  GeneratedIntColumn _constructIbadahId() {
    return GeneratedIntColumn(
      'ibadah_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedTextColumn _data;
  @override
  GeneratedTextColumn get data => _data ??= _constructData();
  GeneratedTextColumn _constructData() {
    return GeneratedTextColumn(
      'data',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, ibadahId, data, created_at];
  @override
  $IbadahsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'ibadahs';
  @override
  final String actualTableName = 'ibadahs';
  @override
  VerificationContext validateIntegrity(IbadahsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.ibadahId.present) {
      context.handle(_ibadahIdMeta,
          ibadahId.isAcceptableValue(d.ibadahId.value, _ibadahIdMeta));
    } else if (ibadahId.isRequired && isInserting) {
      context.missing(_ibadahIdMeta);
    }
    if (d.data.present) {
      context.handle(
          _dataMeta, data.isAcceptableValue(d.data.value, _dataMeta));
    } else if (data.isRequired && isInserting) {
      context.missing(_dataMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Ibadah map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Ibadah.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(IbadahsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.ibadahId.present) {
      map['ibadah_id'] = Variable<int, IntType>(d.ibadahId.value);
    }
    if (d.data.present) {
      map['data'] = Variable<String, StringType>(d.data.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $IbadahsTable createAlias(String alias) {
    return $IbadahsTable(_db, alias);
  }
}

class IbadahPart extends DataClass implements Insertable<IbadahPart> {
  final int id;
  final int ibadahId;
  final int partId;
  final String data;
  final DateTime created_at;
  IbadahPart(
      {@required this.id,
      @required this.ibadahId,
      @required this.partId,
      @required this.data,
      @required this.created_at});
  factory IbadahPart.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return IbadahPart(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      ibadahId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}ibadah_id']),
      partId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}part_id']),
      data: stringType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory IbadahPart.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return IbadahPart(
      id: serializer.fromJson<int>(json['id']),
      ibadahId: serializer.fromJson<int>(json['ibadahId']),
      partId: serializer.fromJson<int>(json['partId']),
      data: serializer.fromJson<String>(json['data']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'ibadahId': serializer.toJson<int>(ibadahId),
      'partId': serializer.toJson<int>(partId),
      'data': serializer.toJson<String>(data),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<IbadahPart>>(bool nullToAbsent) {
    return IbadahPartsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      ibadahId: ibadahId == null && nullToAbsent
          ? const Value.absent()
          : Value(ibadahId),
      partId:
          partId == null && nullToAbsent ? const Value.absent() : Value(partId),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  IbadahPart copyWith(
          {int id,
          int ibadahId,
          int partId,
          String data,
          DateTime created_at}) =>
      IbadahPart(
        id: id ?? this.id,
        ibadahId: ibadahId ?? this.ibadahId,
        partId: partId ?? this.partId,
        data: data ?? this.data,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('IbadahPart(')
          ..write('id: $id, ')
          ..write('ibadahId: $ibadahId, ')
          ..write('partId: $partId, ')
          ..write('data: $data, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(ibadahId.hashCode,
          $mrjc(partId.hashCode, $mrjc(data.hashCode, created_at.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is IbadahPart &&
          other.id == id &&
          other.ibadahId == ibadahId &&
          other.partId == partId &&
          other.data == data &&
          other.created_at == created_at);
}

class IbadahPartsCompanion extends UpdateCompanion<IbadahPart> {
  final Value<int> id;
  final Value<int> ibadahId;
  final Value<int> partId;
  final Value<String> data;
  final Value<DateTime> created_at;
  const IbadahPartsCompanion({
    this.id = const Value.absent(),
    this.ibadahId = const Value.absent(),
    this.partId = const Value.absent(),
    this.data = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  IbadahPartsCompanion copyWith(
      {Value<int> id,
      Value<int> ibadahId,
      Value<int> partId,
      Value<String> data,
      Value<DateTime> created_at}) {
    return IbadahPartsCompanion(
      id: id ?? this.id,
      ibadahId: ibadahId ?? this.ibadahId,
      partId: partId ?? this.partId,
      data: data ?? this.data,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $IbadahPartsTable extends IbadahParts
    with TableInfo<$IbadahPartsTable, IbadahPart> {
  final GeneratedDatabase _db;
  final String _alias;
  $IbadahPartsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _ibadahIdMeta = const VerificationMeta('ibadahId');
  GeneratedIntColumn _ibadahId;
  @override
  GeneratedIntColumn get ibadahId => _ibadahId ??= _constructIbadahId();
  GeneratedIntColumn _constructIbadahId() {
    return GeneratedIntColumn(
      'ibadah_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _partIdMeta = const VerificationMeta('partId');
  GeneratedIntColumn _partId;
  @override
  GeneratedIntColumn get partId => _partId ??= _constructPartId();
  GeneratedIntColumn _constructPartId() {
    return GeneratedIntColumn(
      'part_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedTextColumn _data;
  @override
  GeneratedTextColumn get data => _data ??= _constructData();
  GeneratedTextColumn _constructData() {
    return GeneratedTextColumn(
      'data',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, ibadahId, partId, data, created_at];
  @override
  $IbadahPartsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'ibadah_parts';
  @override
  final String actualTableName = 'ibadah_parts';
  @override
  VerificationContext validateIntegrity(IbadahPartsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.ibadahId.present) {
      context.handle(_ibadahIdMeta,
          ibadahId.isAcceptableValue(d.ibadahId.value, _ibadahIdMeta));
    } else if (ibadahId.isRequired && isInserting) {
      context.missing(_ibadahIdMeta);
    }
    if (d.partId.present) {
      context.handle(
          _partIdMeta, partId.isAcceptableValue(d.partId.value, _partIdMeta));
    } else if (partId.isRequired && isInserting) {
      context.missing(_partIdMeta);
    }
    if (d.data.present) {
      context.handle(
          _dataMeta, data.isAcceptableValue(d.data.value, _dataMeta));
    } else if (data.isRequired && isInserting) {
      context.missing(_dataMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  IbadahPart map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return IbadahPart.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(IbadahPartsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.ibadahId.present) {
      map['ibadah_id'] = Variable<int, IntType>(d.ibadahId.value);
    }
    if (d.partId.present) {
      map['part_id'] = Variable<int, IntType>(d.partId.value);
    }
    if (d.data.present) {
      map['data'] = Variable<String, StringType>(d.data.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $IbadahPartsTable createAlias(String alias) {
    return $IbadahPartsTable(_db, alias);
  }
}

class AudioBook extends DataClass implements Insertable<AudioBook> {
  final int id;
  final int bookId;
  final String data;
  final DateTime created_at;
  AudioBook(
      {@required this.id,
      @required this.bookId,
      @required this.data,
      @required this.created_at});
  factory AudioBook.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return AudioBook(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      bookId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}book_id']),
      data: stringType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      created_at: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory AudioBook.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return AudioBook(
      id: serializer.fromJson<int>(json['id']),
      bookId: serializer.fromJson<int>(json['bookId']),
      data: serializer.fromJson<String>(json['data']),
      created_at: serializer.fromJson<DateTime>(json['created_at']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'bookId': serializer.toJson<int>(bookId),
      'data': serializer.toJson<String>(data),
      'created_at': serializer.toJson<DateTime>(created_at),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<AudioBook>>(bool nullToAbsent) {
    return AudioBooksCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      bookId:
          bookId == null && nullToAbsent ? const Value.absent() : Value(bookId),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      created_at: created_at == null && nullToAbsent
          ? const Value.absent()
          : Value(created_at),
    ) as T;
  }

  AudioBook copyWith({int id, int bookId, String data, DateTime created_at}) =>
      AudioBook(
        id: id ?? this.id,
        bookId: bookId ?? this.bookId,
        data: data ?? this.data,
        created_at: created_at ?? this.created_at,
      );
  @override
  String toString() {
    return (StringBuffer('AudioBook(')
          ..write('id: $id, ')
          ..write('bookId: $bookId, ')
          ..write('data: $data, ')
          ..write('created_at: $created_at')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(bookId.hashCode, $mrjc(data.hashCode, created_at.hashCode))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is AudioBook &&
          other.id == id &&
          other.bookId == bookId &&
          other.data == data &&
          other.created_at == created_at);
}

class AudioBooksCompanion extends UpdateCompanion<AudioBook> {
  final Value<int> id;
  final Value<int> bookId;
  final Value<String> data;
  final Value<DateTime> created_at;
  const AudioBooksCompanion({
    this.id = const Value.absent(),
    this.bookId = const Value.absent(),
    this.data = const Value.absent(),
    this.created_at = const Value.absent(),
  });
  AudioBooksCompanion copyWith(
      {Value<int> id,
      Value<int> bookId,
      Value<String> data,
      Value<DateTime> created_at}) {
    return AudioBooksCompanion(
      id: id ?? this.id,
      bookId: bookId ?? this.bookId,
      data: data ?? this.data,
      created_at: created_at ?? this.created_at,
    );
  }
}

class $AudioBooksTable extends AudioBooks
    with TableInfo<$AudioBooksTable, AudioBook> {
  final GeneratedDatabase _db;
  final String _alias;
  $AudioBooksTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _bookIdMeta = const VerificationMeta('bookId');
  GeneratedIntColumn _bookId;
  @override
  GeneratedIntColumn get bookId => _bookId ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn(
      'book_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedTextColumn _data;
  @override
  GeneratedTextColumn get data => _data ??= _constructData();
  GeneratedTextColumn _constructData() {
    return GeneratedTextColumn(
      'data',
      $tableName,
      false,
    );
  }

  final VerificationMeta _created_atMeta = const VerificationMeta('created_at');
  GeneratedDateTimeColumn _created_at;
  @override
  GeneratedDateTimeColumn get created_at =>
      _created_at ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, bookId, data, created_at];
  @override
  $AudioBooksTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'audio_books';
  @override
  final String actualTableName = 'audio_books';
  @override
  VerificationContext validateIntegrity(AudioBooksCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.bookId.present) {
      context.handle(
          _bookIdMeta, bookId.isAcceptableValue(d.bookId.value, _bookIdMeta));
    } else if (bookId.isRequired && isInserting) {
      context.missing(_bookIdMeta);
    }
    if (d.data.present) {
      context.handle(
          _dataMeta, data.isAcceptableValue(d.data.value, _dataMeta));
    } else if (data.isRequired && isInserting) {
      context.missing(_dataMeta);
    }
    if (d.created_at.present) {
      context.handle(_created_atMeta,
          created_at.isAcceptableValue(d.created_at.value, _created_atMeta));
    } else if (created_at.isRequired && isInserting) {
      context.missing(_created_atMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AudioBook map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return AudioBook.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(AudioBooksCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.bookId.present) {
      map['book_id'] = Variable<int, IntType>(d.bookId.value);
    }
    if (d.data.present) {
      map['data'] = Variable<String, StringType>(d.data.value);
    }
    if (d.created_at.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.created_at.value);
    }
    return map;
  }

  @override
  $AudioBooksTable createAlias(String alias) {
    return $AudioBooksTable(_db, alias);
  }
}

class AudioBookPart extends DataClass implements Insertable<AudioBookPart> {
  final int id;
  final int bookId;
  final int partId;
  final String data;
  final DateTime createdAt;
  AudioBookPart(
      {@required this.id,
      @required this.bookId,
      @required this.partId,
      @required this.data,
      @required this.createdAt});
  factory AudioBookPart.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return AudioBookPart(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      bookId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}book_id']),
      partId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}part_id']),
      data: stringType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory AudioBookPart.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return AudioBookPart(
      id: serializer.fromJson<int>(json['id']),
      bookId: serializer.fromJson<int>(json['bookId']),
      partId: serializer.fromJson<int>(json['partId']),
      data: serializer.fromJson<String>(json['data']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'bookId': serializer.toJson<int>(bookId),
      'partId': serializer.toJson<int>(partId),
      'data': serializer.toJson<String>(data),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<AudioBookPart>>(
      bool nullToAbsent) {
    return AudioBookPartsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      bookId:
          bookId == null && nullToAbsent ? const Value.absent() : Value(bookId),
      partId:
          partId == null && nullToAbsent ? const Value.absent() : Value(partId),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  AudioBookPart copyWith(
          {int id, int bookId, int partId, String data, DateTime createdAt}) =>
      AudioBookPart(
        id: id ?? this.id,
        bookId: bookId ?? this.bookId,
        partId: partId ?? this.partId,
        data: data ?? this.data,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('AudioBookPart(')
          ..write('id: $id, ')
          ..write('bookId: $bookId, ')
          ..write('partId: $partId, ')
          ..write('data: $data, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(bookId.hashCode,
          $mrjc(partId.hashCode, $mrjc(data.hashCode, createdAt.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is AudioBookPart &&
          other.id == id &&
          other.bookId == bookId &&
          other.partId == partId &&
          other.data == data &&
          other.createdAt == createdAt);
}

class AudioBookPartsCompanion extends UpdateCompanion<AudioBookPart> {
  final Value<int> id;
  final Value<int> bookId;
  final Value<int> partId;
  final Value<String> data;
  final Value<DateTime> createdAt;
  const AudioBookPartsCompanion({
    this.id = const Value.absent(),
    this.bookId = const Value.absent(),
    this.partId = const Value.absent(),
    this.data = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  AudioBookPartsCompanion copyWith(
      {Value<int> id,
      Value<int> bookId,
      Value<int> partId,
      Value<String> data,
      Value<DateTime> createdAt}) {
    return AudioBookPartsCompanion(
      id: id ?? this.id,
      bookId: bookId ?? this.bookId,
      partId: partId ?? this.partId,
      data: data ?? this.data,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $AudioBookPartsTable extends AudioBookParts
    with TableInfo<$AudioBookPartsTable, AudioBookPart> {
  final GeneratedDatabase _db;
  final String _alias;
  $AudioBookPartsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _bookIdMeta = const VerificationMeta('bookId');
  GeneratedIntColumn _bookId;
  @override
  GeneratedIntColumn get bookId => _bookId ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn(
      'book_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _partIdMeta = const VerificationMeta('partId');
  GeneratedIntColumn _partId;
  @override
  GeneratedIntColumn get partId => _partId ??= _constructPartId();
  GeneratedIntColumn _constructPartId() {
    return GeneratedIntColumn(
      'part_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedTextColumn _data;
  @override
  GeneratedTextColumn get data => _data ??= _constructData();
  GeneratedTextColumn _constructData() {
    return GeneratedTextColumn(
      'data',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, bookId, partId, data, createdAt];
  @override
  $AudioBookPartsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'audio_book_parts';
  @override
  final String actualTableName = 'audio_book_parts';
  @override
  VerificationContext validateIntegrity(AudioBookPartsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.bookId.present) {
      context.handle(
          _bookIdMeta, bookId.isAcceptableValue(d.bookId.value, _bookIdMeta));
    } else if (bookId.isRequired && isInserting) {
      context.missing(_bookIdMeta);
    }
    if (d.partId.present) {
      context.handle(
          _partIdMeta, partId.isAcceptableValue(d.partId.value, _partIdMeta));
    } else if (partId.isRequired && isInserting) {
      context.missing(_partIdMeta);
    }
    if (d.data.present) {
      context.handle(
          _dataMeta, data.isAcceptableValue(d.data.value, _dataMeta));
    } else if (data.isRequired && isInserting) {
      context.missing(_dataMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  AudioBookPart map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return AudioBookPart.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(AudioBookPartsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.bookId.present) {
      map['book_id'] = Variable<int, IntType>(d.bookId.value);
    }
    if (d.partId.present) {
      map['part_id'] = Variable<int, IntType>(d.partId.value);
    }
    if (d.data.present) {
      map['data'] = Variable<String, StringType>(d.data.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $AudioBookPartsTable createAlias(String alias) {
    return $AudioBookPartsTable(_db, alias);
  }
}

class DownloadedSongData extends DataClass
    implements Insertable<DownloadedSongData> {
  final int id;
  final int songId;
  final String localFilePath;
  final DateTime createdAt;
  DownloadedSongData(
      {@required this.id,
      @required this.songId,
      @required this.localFilePath,
      @required this.createdAt});
  factory DownloadedSongData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DownloadedSongData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      songId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}song_id']),
      localFilePath: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}local_file_path']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory DownloadedSongData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return DownloadedSongData(
      id: serializer.fromJson<int>(json['id']),
      songId: serializer.fromJson<int>(json['songId']),
      localFilePath: serializer.fromJson<String>(json['localFilePath']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'songId': serializer.toJson<int>(songId),
      'localFilePath': serializer.toJson<String>(localFilePath),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<DownloadedSongData>>(
      bool nullToAbsent) {
    return DownloadedSongCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      songId:
          songId == null && nullToAbsent ? const Value.absent() : Value(songId),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  DownloadedSongData copyWith(
          {int id, int songId, String localFilePath, DateTime createdAt}) =>
      DownloadedSongData(
        id: id ?? this.id,
        songId: songId ?? this.songId,
        localFilePath: localFilePath ?? this.localFilePath,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedSongData(')
          ..write('id: $id, ')
          ..write('songId: $songId, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          songId.hashCode, $mrjc(localFilePath.hashCode, createdAt.hashCode))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is DownloadedSongData &&
          other.id == id &&
          other.songId == songId &&
          other.localFilePath == localFilePath &&
          other.createdAt == createdAt);
}

class DownloadedSongCompanion extends UpdateCompanion<DownloadedSongData> {
  final Value<int> id;
  final Value<int> songId;
  final Value<String> localFilePath;
  final Value<DateTime> createdAt;
  const DownloadedSongCompanion({
    this.id = const Value.absent(),
    this.songId = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  DownloadedSongCompanion copyWith(
      {Value<int> id,
      Value<int> songId,
      Value<String> localFilePath,
      Value<DateTime> createdAt}) {
    return DownloadedSongCompanion(
      id: id ?? this.id,
      songId: songId ?? this.songId,
      localFilePath: localFilePath ?? this.localFilePath,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $DownloadedSongTable extends DownloadedSong
    with TableInfo<$DownloadedSongTable, DownloadedSongData> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedSongTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _songIdMeta = const VerificationMeta('songId');
  GeneratedIntColumn _songId;
  @override
  GeneratedIntColumn get songId => _songId ??= _constructSongId();
  GeneratedIntColumn _constructSongId() {
    return GeneratedIntColumn(
      'song_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  GeneratedTextColumn _localFilePath;
  @override
  GeneratedTextColumn get localFilePath =>
      _localFilePath ??= _constructLocalFilePath();
  GeneratedTextColumn _constructLocalFilePath() {
    return GeneratedTextColumn(
      'local_file_path',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, songId, localFilePath, createdAt];
  @override
  $DownloadedSongTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_song';
  @override
  final String actualTableName = 'downloaded_song';
  @override
  VerificationContext validateIntegrity(DownloadedSongCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.songId.present) {
      context.handle(
          _songIdMeta, songId.isAcceptableValue(d.songId.value, _songIdMeta));
    } else if (songId.isRequired && isInserting) {
      context.missing(_songIdMeta);
    }
    if (d.localFilePath.present) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableValue(
              d.localFilePath.value, _localFilePathMeta));
    } else if (localFilePath.isRequired && isInserting) {
      context.missing(_localFilePathMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DownloadedSongData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedSongData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedSongCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.songId.present) {
      map['song_id'] = Variable<int, IntType>(d.songId.value);
    }
    if (d.localFilePath.present) {
      map['local_file_path'] =
          Variable<String, StringType>(d.localFilePath.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $DownloadedSongTable createAlias(String alias) {
    return $DownloadedSongTable(_db, alias);
  }
}

class DownloadedIbadahData extends DataClass
    implements Insertable<DownloadedIbadahData> {
  final int id;
  final int ibadahId;
  final DateTime createdAt;
  DownloadedIbadahData(
      {@required this.id, @required this.ibadahId, @required this.createdAt});
  factory DownloadedIbadahData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DownloadedIbadahData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      ibadahId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}ibadah_id']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory DownloadedIbadahData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return DownloadedIbadahData(
      id: serializer.fromJson<int>(json['id']),
      ibadahId: serializer.fromJson<int>(json['ibadahId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'ibadahId': serializer.toJson<int>(ibadahId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<DownloadedIbadahData>>(
      bool nullToAbsent) {
    return DownloadedIbadahCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      ibadahId: ibadahId == null && nullToAbsent
          ? const Value.absent()
          : Value(ibadahId),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  DownloadedIbadahData copyWith({int id, int ibadahId, DateTime createdAt}) =>
      DownloadedIbadahData(
        id: id ?? this.id,
        ibadahId: ibadahId ?? this.ibadahId,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedIbadahData(')
          ..write('id: $id, ')
          ..write('ibadahId: $ibadahId, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(ibadahId.hashCode, createdAt.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is DownloadedIbadahData &&
          other.id == id &&
          other.ibadahId == ibadahId &&
          other.createdAt == createdAt);
}

class DownloadedIbadahCompanion extends UpdateCompanion<DownloadedIbadahData> {
  final Value<int> id;
  final Value<int> ibadahId;
  final Value<DateTime> createdAt;
  const DownloadedIbadahCompanion({
    this.id = const Value.absent(),
    this.ibadahId = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  DownloadedIbadahCompanion copyWith(
      {Value<int> id, Value<int> ibadahId, Value<DateTime> createdAt}) {
    return DownloadedIbadahCompanion(
      id: id ?? this.id,
      ibadahId: ibadahId ?? this.ibadahId,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $DownloadedIbadahTable extends DownloadedIbadah
    with TableInfo<$DownloadedIbadahTable, DownloadedIbadahData> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedIbadahTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _ibadahIdMeta = const VerificationMeta('ibadahId');
  GeneratedIntColumn _ibadahId;
  @override
  GeneratedIntColumn get ibadahId => _ibadahId ??= _constructIbadahId();
  GeneratedIntColumn _constructIbadahId() {
    return GeneratedIntColumn(
      'ibadah_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, ibadahId, createdAt];
  @override
  $DownloadedIbadahTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_ibadah';
  @override
  final String actualTableName = 'downloaded_ibadah';
  @override
  VerificationContext validateIntegrity(DownloadedIbadahCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.ibadahId.present) {
      context.handle(_ibadahIdMeta,
          ibadahId.isAcceptableValue(d.ibadahId.value, _ibadahIdMeta));
    } else if (ibadahId.isRequired && isInserting) {
      context.missing(_ibadahIdMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DownloadedIbadahData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedIbadahData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedIbadahCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.ibadahId.present) {
      map['ibadah_id'] = Variable<int, IntType>(d.ibadahId.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $DownloadedIbadahTable createAlias(String alias) {
    return $DownloadedIbadahTable(_db, alias);
  }
}

class DownloadedIbadahPartData extends DataClass
    implements Insertable<DownloadedIbadahPartData> {
  final int id;
  final int ibadahId;
  final int partId;
  final String localFilePath;
  final DateTime createdAt;
  DownloadedIbadahPartData(
      {@required this.id,
      @required this.ibadahId,
      @required this.partId,
      @required this.localFilePath,
      @required this.createdAt});
  factory DownloadedIbadahPartData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DownloadedIbadahPartData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      ibadahId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}ibadah_id']),
      partId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}part_id']),
      localFilePath: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}local_file_path']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory DownloadedIbadahPartData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return DownloadedIbadahPartData(
      id: serializer.fromJson<int>(json['id']),
      ibadahId: serializer.fromJson<int>(json['ibadahId']),
      partId: serializer.fromJson<int>(json['partId']),
      localFilePath: serializer.fromJson<String>(json['localFilePath']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'ibadahId': serializer.toJson<int>(ibadahId),
      'partId': serializer.toJson<int>(partId),
      'localFilePath': serializer.toJson<String>(localFilePath),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<DownloadedIbadahPartData>>(
      bool nullToAbsent) {
    return DownloadedIbadahPartCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      ibadahId: ibadahId == null && nullToAbsent
          ? const Value.absent()
          : Value(ibadahId),
      partId:
          partId == null && nullToAbsent ? const Value.absent() : Value(partId),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  DownloadedIbadahPartData copyWith(
          {int id,
          int ibadahId,
          int partId,
          String localFilePath,
          DateTime createdAt}) =>
      DownloadedIbadahPartData(
        id: id ?? this.id,
        ibadahId: ibadahId ?? this.ibadahId,
        partId: partId ?? this.partId,
        localFilePath: localFilePath ?? this.localFilePath,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedIbadahPartData(')
          ..write('id: $id, ')
          ..write('ibadahId: $ibadahId, ')
          ..write('partId: $partId, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          ibadahId.hashCode,
          $mrjc(partId.hashCode,
              $mrjc(localFilePath.hashCode, createdAt.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is DownloadedIbadahPartData &&
          other.id == id &&
          other.ibadahId == ibadahId &&
          other.partId == partId &&
          other.localFilePath == localFilePath &&
          other.createdAt == createdAt);
}

class DownloadedIbadahPartCompanion
    extends UpdateCompanion<DownloadedIbadahPartData> {
  final Value<int> id;
  final Value<int> ibadahId;
  final Value<int> partId;
  final Value<String> localFilePath;
  final Value<DateTime> createdAt;
  const DownloadedIbadahPartCompanion({
    this.id = const Value.absent(),
    this.ibadahId = const Value.absent(),
    this.partId = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  DownloadedIbadahPartCompanion copyWith(
      {Value<int> id,
      Value<int> ibadahId,
      Value<int> partId,
      Value<String> localFilePath,
      Value<DateTime> createdAt}) {
    return DownloadedIbadahPartCompanion(
      id: id ?? this.id,
      ibadahId: ibadahId ?? this.ibadahId,
      partId: partId ?? this.partId,
      localFilePath: localFilePath ?? this.localFilePath,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $DownloadedIbadahPartTable extends DownloadedIbadahPart
    with TableInfo<$DownloadedIbadahPartTable, DownloadedIbadahPartData> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedIbadahPartTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _ibadahIdMeta = const VerificationMeta('ibadahId');
  GeneratedIntColumn _ibadahId;
  @override
  GeneratedIntColumn get ibadahId => _ibadahId ??= _constructIbadahId();
  GeneratedIntColumn _constructIbadahId() {
    return GeneratedIntColumn(
      'ibadah_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _partIdMeta = const VerificationMeta('partId');
  GeneratedIntColumn _partId;
  @override
  GeneratedIntColumn get partId => _partId ??= _constructPartId();
  GeneratedIntColumn _constructPartId() {
    return GeneratedIntColumn(
      'part_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  GeneratedTextColumn _localFilePath;
  @override
  GeneratedTextColumn get localFilePath =>
      _localFilePath ??= _constructLocalFilePath();
  GeneratedTextColumn _constructLocalFilePath() {
    return GeneratedTextColumn(
      'local_file_path',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, ibadahId, partId, localFilePath, createdAt];
  @override
  $DownloadedIbadahPartTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_ibadah_part';
  @override
  final String actualTableName = 'downloaded_ibadah_part';
  @override
  VerificationContext validateIntegrity(DownloadedIbadahPartCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.ibadahId.present) {
      context.handle(_ibadahIdMeta,
          ibadahId.isAcceptableValue(d.ibadahId.value, _ibadahIdMeta));
    } else if (ibadahId.isRequired && isInserting) {
      context.missing(_ibadahIdMeta);
    }
    if (d.partId.present) {
      context.handle(
          _partIdMeta, partId.isAcceptableValue(d.partId.value, _partIdMeta));
    } else if (partId.isRequired && isInserting) {
      context.missing(_partIdMeta);
    }
    if (d.localFilePath.present) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableValue(
              d.localFilePath.value, _localFilePathMeta));
    } else if (localFilePath.isRequired && isInserting) {
      context.missing(_localFilePathMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DownloadedIbadahPartData map(Map<String, dynamic> data,
      {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedIbadahPartData.fromData(data, _db,
        prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedIbadahPartCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.ibadahId.present) {
      map['ibadah_id'] = Variable<int, IntType>(d.ibadahId.value);
    }
    if (d.partId.present) {
      map['part_id'] = Variable<int, IntType>(d.partId.value);
    }
    if (d.localFilePath.present) {
      map['local_file_path'] =
          Variable<String, StringType>(d.localFilePath.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $DownloadedIbadahPartTable createAlias(String alias) {
    return $DownloadedIbadahPartTable(_db, alias);
  }
}

class DownloadedBookData extends DataClass
    implements Insertable<DownloadedBookData> {
  final int id;
  final int bookId;
  final DateTime createdAt;
  DownloadedBookData(
      {@required this.id, @required this.bookId, @required this.createdAt});
  factory DownloadedBookData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DownloadedBookData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      bookId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}book_id']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory DownloadedBookData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return DownloadedBookData(
      id: serializer.fromJson<int>(json['id']),
      bookId: serializer.fromJson<int>(json['bookId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'bookId': serializer.toJson<int>(bookId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<DownloadedBookData>>(
      bool nullToAbsent) {
    return DownloadedBookCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      bookId:
          bookId == null && nullToAbsent ? const Value.absent() : Value(bookId),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  DownloadedBookData copyWith({int id, int bookId, DateTime createdAt}) =>
      DownloadedBookData(
        id: id ?? this.id,
        bookId: bookId ?? this.bookId,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedBookData(')
          ..write('id: $id, ')
          ..write('bookId: $bookId, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(bookId.hashCode, createdAt.hashCode)));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is DownloadedBookData &&
          other.id == id &&
          other.bookId == bookId &&
          other.createdAt == createdAt);
}

class DownloadedBookCompanion extends UpdateCompanion<DownloadedBookData> {
  final Value<int> id;
  final Value<int> bookId;
  final Value<DateTime> createdAt;
  const DownloadedBookCompanion({
    this.id = const Value.absent(),
    this.bookId = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  DownloadedBookCompanion copyWith(
      {Value<int> id, Value<int> bookId, Value<DateTime> createdAt}) {
    return DownloadedBookCompanion(
      id: id ?? this.id,
      bookId: bookId ?? this.bookId,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $DownloadedBookTable extends DownloadedBook
    with TableInfo<$DownloadedBookTable, DownloadedBookData> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedBookTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _bookIdMeta = const VerificationMeta('bookId');
  GeneratedIntColumn _bookId;
  @override
  GeneratedIntColumn get bookId => _bookId ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn(
      'book_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, bookId, createdAt];
  @override
  $DownloadedBookTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_book';
  @override
  final String actualTableName = 'downloaded_book';
  @override
  VerificationContext validateIntegrity(DownloadedBookCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.bookId.present) {
      context.handle(
          _bookIdMeta, bookId.isAcceptableValue(d.bookId.value, _bookIdMeta));
    } else if (bookId.isRequired && isInserting) {
      context.missing(_bookIdMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DownloadedBookData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedBookData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedBookCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.bookId.present) {
      map['book_id'] = Variable<int, IntType>(d.bookId.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $DownloadedBookTable createAlias(String alias) {
    return $DownloadedBookTable(_db, alias);
  }
}

class DownloadedBookPartData extends DataClass
    implements Insertable<DownloadedBookPartData> {
  final int id;
  final int bookId;
  final int partId;
  final String localFilePath;
  final DateTime createdAt;
  DownloadedBookPartData(
      {@required this.id,
      @required this.bookId,
      @required this.partId,
      @required this.localFilePath,
      @required this.createdAt});
  factory DownloadedBookPartData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DownloadedBookPartData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      bookId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}book_id']),
      partId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}part_id']),
      localFilePath: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}local_file_path']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
    );
  }
  factory DownloadedBookPartData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return DownloadedBookPartData(
      id: serializer.fromJson<int>(json['id']),
      bookId: serializer.fromJson<int>(json['bookId']),
      partId: serializer.fromJson<int>(json['partId']),
      localFilePath: serializer.fromJson<String>(json['localFilePath']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'bookId': serializer.toJson<int>(bookId),
      'partId': serializer.toJson<int>(partId),
      'localFilePath': serializer.toJson<String>(localFilePath),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<DownloadedBookPartData>>(
      bool nullToAbsent) {
    return DownloadedBookPartCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      bookId:
          bookId == null && nullToAbsent ? const Value.absent() : Value(bookId),
      partId:
          partId == null && nullToAbsent ? const Value.absent() : Value(partId),
      localFilePath: localFilePath == null && nullToAbsent
          ? const Value.absent()
          : Value(localFilePath),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
    ) as T;
  }

  DownloadedBookPartData copyWith(
          {int id,
          int bookId,
          int partId,
          String localFilePath,
          DateTime createdAt}) =>
      DownloadedBookPartData(
        id: id ?? this.id,
        bookId: bookId ?? this.bookId,
        partId: partId ?? this.partId,
        localFilePath: localFilePath ?? this.localFilePath,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('DownloadedBookPartData(')
          ..write('id: $id, ')
          ..write('bookId: $bookId, ')
          ..write('partId: $partId, ')
          ..write('localFilePath: $localFilePath, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          bookId.hashCode,
          $mrjc(partId.hashCode,
              $mrjc(localFilePath.hashCode, createdAt.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is DownloadedBookPartData &&
          other.id == id &&
          other.bookId == bookId &&
          other.partId == partId &&
          other.localFilePath == localFilePath &&
          other.createdAt == createdAt);
}

class DownloadedBookPartCompanion
    extends UpdateCompanion<DownloadedBookPartData> {
  final Value<int> id;
  final Value<int> bookId;
  final Value<int> partId;
  final Value<String> localFilePath;
  final Value<DateTime> createdAt;
  const DownloadedBookPartCompanion({
    this.id = const Value.absent(),
    this.bookId = const Value.absent(),
    this.partId = const Value.absent(),
    this.localFilePath = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  DownloadedBookPartCompanion copyWith(
      {Value<int> id,
      Value<int> bookId,
      Value<int> partId,
      Value<String> localFilePath,
      Value<DateTime> createdAt}) {
    return DownloadedBookPartCompanion(
      id: id ?? this.id,
      bookId: bookId ?? this.bookId,
      partId: partId ?? this.partId,
      localFilePath: localFilePath ?? this.localFilePath,
      createdAt: createdAt ?? this.createdAt,
    );
  }
}

class $DownloadedBookPartTable extends DownloadedBookPart
    with TableInfo<$DownloadedBookPartTable, DownloadedBookPartData> {
  final GeneratedDatabase _db;
  final String _alias;
  $DownloadedBookPartTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _bookIdMeta = const VerificationMeta('bookId');
  GeneratedIntColumn _bookId;
  @override
  GeneratedIntColumn get bookId => _bookId ??= _constructBookId();
  GeneratedIntColumn _constructBookId() {
    return GeneratedIntColumn(
      'book_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _partIdMeta = const VerificationMeta('partId');
  GeneratedIntColumn _partId;
  @override
  GeneratedIntColumn get partId => _partId ??= _constructPartId();
  GeneratedIntColumn _constructPartId() {
    return GeneratedIntColumn(
      'part_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _localFilePathMeta =
      const VerificationMeta('localFilePath');
  GeneratedTextColumn _localFilePath;
  @override
  GeneratedTextColumn get localFilePath =>
      _localFilePath ??= _constructLocalFilePath();
  GeneratedTextColumn _constructLocalFilePath() {
    return GeneratedTextColumn(
      'local_file_path',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, bookId, partId, localFilePath, createdAt];
  @override
  $DownloadedBookPartTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'downloaded_book_part';
  @override
  final String actualTableName = 'downloaded_book_part';
  @override
  VerificationContext validateIntegrity(DownloadedBookPartCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.bookId.present) {
      context.handle(
          _bookIdMeta, bookId.isAcceptableValue(d.bookId.value, _bookIdMeta));
    } else if (bookId.isRequired && isInserting) {
      context.missing(_bookIdMeta);
    }
    if (d.partId.present) {
      context.handle(
          _partIdMeta, partId.isAcceptableValue(d.partId.value, _partIdMeta));
    } else if (partId.isRequired && isInserting) {
      context.missing(_partIdMeta);
    }
    if (d.localFilePath.present) {
      context.handle(
          _localFilePathMeta,
          localFilePath.isAcceptableValue(
              d.localFilePath.value, _localFilePathMeta));
    } else if (localFilePath.isRequired && isInserting) {
      context.missing(_localFilePathMeta);
    }
    if (d.createdAt.present) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableValue(d.createdAt.value, _createdAtMeta));
    } else if (createdAt.isRequired && isInserting) {
      context.missing(_createdAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DownloadedBookPartData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DownloadedBookPartData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(DownloadedBookPartCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.bookId.present) {
      map['book_id'] = Variable<int, IntType>(d.bookId.value);
    }
    if (d.partId.present) {
      map['part_id'] = Variable<int, IntType>(d.partId.value);
    }
    if (d.localFilePath.present) {
      map['local_file_path'] =
          Variable<String, StringType>(d.localFilePath.value);
    }
    if (d.createdAt.present) {
      map['created_at'] = Variable<DateTime, DateTimeType>(d.createdAt.value);
    }
    return map;
  }

  @override
  $DownloadedBookPartTable createAlias(String alias) {
    return $DownloadedBookPartTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(const SqlTypeSystem.withDefaults(), e);
  $SongsTable _songs;
  $SongsTable get songs => _songs ??= $SongsTable(this);
  $PlaylistsTable _playlists;
  $PlaylistsTable get playlists => _playlists ??= $PlaylistsTable(this);
  $JudulPlaylistsTable _judulPlaylists;
  $JudulPlaylistsTable get judulPlaylists =>
      _judulPlaylists ??= $JudulPlaylistsTable(this);
  $FavoritsTable _favorits;
  $FavoritsTable get favorits => _favorits ??= $FavoritsTable(this);
  $InboxsTable _inboxs;
  $InboxsTable get inboxs => _inboxs ??= $InboxsTable(this);
  $RecentSongsTable _recentSongs;
  $RecentSongsTable get recentSongs => _recentSongs ??= $RecentSongsTable(this);
  $RecentBooksTable _recentBooks;
  $RecentBooksTable get recentBooks => _recentBooks ??= $RecentBooksTable(this);
  $IbadahsTable _ibadahs;
  $IbadahsTable get ibadahs => _ibadahs ??= $IbadahsTable(this);
  $IbadahPartsTable _ibadahParts;
  $IbadahPartsTable get ibadahParts => _ibadahParts ??= $IbadahPartsTable(this);
  $AudioBooksTable _audioBooks;
  $AudioBooksTable get audioBooks => _audioBooks ??= $AudioBooksTable(this);
  $AudioBookPartsTable _audioBookParts;
  $AudioBookPartsTable get audioBookParts =>
      _audioBookParts ??= $AudioBookPartsTable(this);
  $DownloadedSongTable _downloadedSong;
  $DownloadedSongTable get downloadedSong =>
      _downloadedSong ??= $DownloadedSongTable(this);
  $DownloadedIbadahTable _downloadedIbadah;
  $DownloadedIbadahTable get downloadedIbadah =>
      _downloadedIbadah ??= $DownloadedIbadahTable(this);
  $DownloadedIbadahPartTable _downloadedIbadahPart;
  $DownloadedIbadahPartTable get downloadedIbadahPart =>
      _downloadedIbadahPart ??= $DownloadedIbadahPartTable(this);
  $DownloadedBookTable _downloadedBook;
  $DownloadedBookTable get downloadedBook =>
      _downloadedBook ??= $DownloadedBookTable(this);
  $DownloadedBookPartTable _downloadedBookPart;
  $DownloadedBookPartTable get downloadedBookPart =>
      _downloadedBookPart ??= $DownloadedBookPartTable(this);
  @override
  List<TableInfo> get allTables => [
        songs,
        playlists,
        judulPlaylists,
        favorits,
        inboxs,
        recentSongs,
        recentBooks,
        ibadahs,
        ibadahParts,
        audioBooks,
        audioBookParts,
        downloadedSong,
        downloadedIbadah,
        downloadedIbadahPart,
        downloadedBook,
        downloadedBookPart
      ];
}
