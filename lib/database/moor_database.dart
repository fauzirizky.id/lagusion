import 'dart:convert';

import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:music_mulai/bloc/audio_book_model.dart';
import 'package:music_mulai/bloc/audio_model.dart';
import 'package:music_mulai/bloc/ibadah_model.dart';

// Moor works by source gen. This file will all the generated code.
part 'moor_database.g.dart';

// The name of the database table is "tasks"
// By default, the name of the generated data class will be "Task" (without "s")
class Songs extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get idMusic => integer()();
  TextColumn get data => text()();
  IntColumn get musicVersion => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class Ibadahs extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get ibadahId => integer()();
  TextColumn get data => text()();
  DateTimeColumn get created_at => dateTime()();
}

class IbadahParts extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get ibadahId => integer()();
  IntColumn get partId => integer()();
  TextColumn get data => text()();
  DateTimeColumn get created_at => dateTime()();
}

class AudioBooks extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get bookId => integer()();
  TextColumn get data => text()();
  DateTimeColumn get created_at => dateTime()();
}

class AudioBookParts extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get bookId => integer()();
  IntColumn get partId => integer()();
  TextColumn get data => text()();
  DateTimeColumn get createdAt => dateTime()();
}

class Playlists extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get playlist_id => integer()();
  IntColumn get music_id => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class JudulPlaylists extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get judul_playlist => text().withLength(min: 1, max: 250)();
  DateTimeColumn get created_at => dateTime()();
}

class Favorits extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get music_id => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class Inboxs extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get inbox_id => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class RecentSongs extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get music_id => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class RecentBooks extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get book_id => integer()();
  DateTimeColumn get created_at => dateTime()();
}

class DownloadedSong extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get songId => integer()();
  TextColumn get localFilePath => text()();
  DateTimeColumn get createdAt => dateTime()();
}

class DownloadedIbadah extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get ibadahId => integer()();
  DateTimeColumn get createdAt => dateTime()();
}

class DownloadedBook extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get bookId => integer()();
  DateTimeColumn get createdAt => dateTime()();
}

class DownloadedIbadahPart extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get ibadahId => integer()();
  IntColumn get partId => integer()();
  TextColumn get localFilePath => text()();
  DateTimeColumn get createdAt => dateTime()();
}

class DownloadedBookPart extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get bookId => integer()();
  IntColumn get partId => integer()();
  TextColumn get localFilePath => text()();
  DateTimeColumn get createdAt => dateTime()();
}

// This annotation tells the code generator which tables this DB works with
@UseMoor(tables: [
  Songs,
  Playlists,
  JudulPlaylists,
  Favorits,
  Inboxs,
  RecentSongs,
  RecentBooks,
  Ibadahs,
  IbadahParts,
  AudioBooks,
  AudioBookParts,
  DownloadedSong,
  DownloadedIbadah,
  DownloadedIbadahPart,
  DownloadedBook,
  DownloadedBookPart
])
// _$AppDatabase is the name of the generated class
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      // Specify the location of the database file
      : super((FlutterQueryExecutor.inDatabaseFolder(
          path: 'db.sqlite',
          // Good for debugging - prints SQL in the console
          logStatements: true,
        )));

  // Bump this when changing tables and columns.
  // Migrations will be covered in the next part.
  @override
  int get schemaVersion => 1;

  Future<List<Song>> getAllSongs() => select(songs).get();
  Future<List<Playlist>> getAllPlaylists() => select(playlists).get();
  Future<List<RecentSong>> getAllRecentSong() => select(recentSongs).get();
  Future<List<JudulPlaylist>> getAllJudulPlaylists() =>
      select(judulPlaylists).get();
  Future<List<Favorit>> getAllFavorits() => select(favorits).get();
  Future<List<Favorit>> getWhereFavorits(id) {
    return (select(favorits)..where((t) => t.music_id.equals(id))).get();
  }

  Future<List<Inbox>> getAllInboxs() => select(inboxs).get();
  Future<List<Inbox>> getWhereInboxs(id) {
    return (select(inboxs)..where((t) => t.inbox_id.equals(id))).get();
  }

  Stream<List<Song>> watchAllSongs() => select(songs).watch();
  Stream<List<Playlist>> watchAllPlaylists() => select(playlists).watch();
  Stream<List<RecentSong>> watchAllRecentSong() => select(recentSongs).watch();
  Stream<List<RecentSong>> watchWhereRecentSong(id) {
    return (select(recentSongs)..where((t) => t.music_id.equals(id))).watch();
  }

  Stream<List<SongModel>> watchAllSongFromRecentSong() {
    final query = select(recentSongs).join(
        [leftOuterJoin(songs, songs.idMusic.equalsExp(recentSongs.music_id))]);
    return query.watch().map((rows) => rows.map((row) {
          return SongModel.fromJson(jsonDecode(
            row.readTable(songs).data,
          ));
        }).toList());
  }

  Stream<List<RecentBook>> watchAllRecentBook() => select(recentBooks).watch();

  Stream<List<JudulPlaylist>> watchAllJudulPlaylists() =>
      select(judulPlaylists).watch();
  Stream<List<Favorit>> watchAllFavorits() => select(favorits).watch();
  Stream<List<Favorit>> watchWhereFavorits(id) {
    return (select(favorits)..where((t) => t.music_id.equals(id))).watch();
  }

  Stream<List<SongModel>> watchAllFavoritesSong() {
    final query = select(favorits).join(
        [leftOuterJoin(songs, songs.idMusic.equalsExp(favorits.music_id))]);
    return query.watch().map((rows) => rows.map((row) {
          return SongModel.fromJson(jsonDecode(
            row.readTable(songs).data,
          ));
        }).toList());
  }

  Stream<List<Inbox>> watchAllInboxs() => select(inboxs).watch();
  Stream<List<Inbox>> watchWhereInboxs(id) {
    return (select(inboxs)..where((t) => t.inbox_id.equals(id))).watch();
  }

  Stream<List<Playlist>> watchWherePlaylist(id, music_id) {
    return (select(playlists)
          ..where((t) => t.playlist_id.equals(id))
          ..where((a) => a.music_id.equals(music_id)))
        .watch();
  }

  Stream<List<Playlist>> watchWhereRecentPlaylist(id) {
    return (select(playlists)..where((t) => t.music_id.equals(id))).watch();
  }

  Stream<List<Playlist>> watchAllRecentPlaylist() => select(playlists).watch();

  Stream<List<RecentBook>> watchWhereRecentBook(id) {
    return (select(recentBooks)..where((t) => t.book_id.equals(id))).watch();
  }

  Stream<List<Ibadah>> watchAllIbadah() => select(ibadahs).watch();
  Stream<List<IbadahPart>> watchWhereIbadahPart(id) {
    return (select(ibadahParts)..where((t) => t.ibadahId.equals(id))).watch();
  }

  Stream<List<AudioBook>> watchAllAudioBook() => select(audioBooks).watch();
  Stream<List<AudioBookPart>> watchWhereAudioBookPart(id) {
    return (select(audioBookParts)..where((t) => t.bookId.equals(id)))
        .watch();
  }

  Future insertIbadah(Ibadah ibadah) async {
    final query = select(ibadahs)
      ..where((tbl) => tbl.ibadahId.equals(ibadah.ibadahId));
    final listIbadah = await query.get();
    if (listIbadah.isEmpty) {
      print('NEW IBADAH PART');
      into(ibadahs).insert(ibadah);
    } else {
      print('IBADAH PART EXIST AT DB');
    }
  }
  Future updateIbadah(Ibadah ibadah) => update(ibadahs).replace(ibadah);
  Future deleteIbadah(Ibadah ibadah) => delete(ibadahs).delete(ibadah);

  Future insertIbadahParts(IbadahPart ibadahPart) async {
    final query = select(ibadahParts)
      ..where((tbl) => tbl.partId.equals(ibadahPart.partId));
    final listPart = await query.get();
    if (listPart.isEmpty) {
      print('NEW IBADAH PART');
      into(ibadahParts).insert(ibadahPart);
    } else {
      print('IBADAH PART EXIST AT DB');
    }
  }

  Future insertBookParts(AudioBookPart part) async {
    final query = select(audioBookParts)
      ..where((tbl) => tbl.partId.equals(part.partId));
    final listPart = await query.get();
    if (listPart.isEmpty) {
      print('NEW IBADAH PART');
      into(audioBookParts).insert(part);
    } else {
      print('IBADAH PART EXIST AT DB');
    }
  }

  Future updateIbadahParts(IbadahPart ibadahPart) =>
      update(ibadahParts).replace(ibadahPart);
  Future deleteIbadahParts(IbadahPart ibadahPart) =>
      delete(ibadahParts).delete(ibadahPart);
  Future insertAudioBookParts(AudioBookPart audioBookPart) =>
      into(audioBookParts).insert(audioBookPart);
  Future updateAudioBookParts(AudioBookPart audioBookPart) =>
      update(audioBookParts).replace(audioBookPart);
  Future deleteAudioBookParts(AudioBookPart audioBookPart) =>
      delete(audioBookParts).delete(audioBookPart);

  Future insertAudioBook(AudioBook audioBook) async {
    final query = select(audioBooks)
      ..where((tbl) => tbl.bookId.equals(audioBook.bookId));
    final listPart = await query.get();
    if (listPart.isEmpty) {
      print('NEW IBADAH PART');
      into(audioBooks).insert(audioBook);
    } else {
      print('IBADAH PART EXIST AT DB');
    }
  }

  Future updateAudioBook(AudioBook audioBook) =>
      update(audioBooks).replace(audioBook);
  Future deleteAudioBook(AudioBook audioBook) =>
      delete(audioBooks).delete(audioBook);

  Future insertSong(Song song) async {
    final query = select(songs)
      ..where((tbl) => tbl.musicVersion.equals(song.musicVersion));
    query.where((tbl) => tbl.idMusic.equals(song.idMusic));
    final listSong = await query.get();
    if (listSong.isEmpty) {
      print('NEW SONG');
      into(songs).insert(song);
    } else {
      print('SONGS EXIST AT SONG DB');
    }
  }

  Future updateSong(Song song) => update(songs).replace(song);
  Future deleteSong(Song song) => delete(songs).delete(song);

  Future insertRecentSong(RecentSong recentSong) async {
    final query = select(recentSongs)
      ..where((tbl) => tbl.music_id.equals(recentSong.music_id));
    final listRecentSong = await query.get();
    if (listRecentSong.isEmpty) {
      into(recentSongs).insert(recentSong);
    } else {
      updateRecentSong(recentSong);
    }
  }

  Future updateRecentSong(RecentSong recentSong) =>
      update(recentSongs).replace(recentSong);
  Future deleteRecentSong(RecentSong recentSong) =>
      delete(recentSongs).delete(recentSong);
  Future insertRecentBook(RecentBook recentBook) =>
      into(recentBooks).insert(recentBook);
  Future updateRecentBook(RecentBook recentBook) =>
      update(recentBooks).replace(recentBook);
  Future deleteRecentBook(RecentBook recentBook) =>
      delete(recentBooks).delete(recentBook);

  Future insertPlaylist(Playlist playlist) => into(playlists).insert(playlist);
  Future updatePlaylist(Playlist playlist) =>
      update(playlists).replace(playlist);
  Future deletePlaylist(Playlist playlist) =>
      delete(playlists).delete(playlist);
  Future<int> countPlaylist() =>
      select(playlists).get().then((value) => value.length);

  Future insertJudulPlaylist(JudulPlaylist judulPlaylist) =>
      into(judulPlaylists).insert(judulPlaylist);
  Future updateJudulPlaylist(JudulPlaylist judulPlaylist) =>
      update(judulPlaylists).replace(judulPlaylist);
  Future deleteJudulPlaylist(JudulPlaylist judulPlaylist) =>
      delete(judulPlaylists).delete(judulPlaylist);
  Future insertFavorit(Favorit favorit) => into(favorits).insert(favorit);
  Future updateFavorit(Favorit favorit) => update(favorits).replace(favorit);
  Future deleteFavorit(Favorit favorit) => delete(favorits).delete(favorit);
  Future countFavorits() async => (await select(favorits).get()).length;
  Stream<int> watchCounterFavorites() =>
      select(favorits).watch().map((event) => event.length);

  Stream<int> watchCounterPlayList(int id) =>
      (select(playlists)..where((t) => t.playlist_id.equals(id)))
          .watch()
          .map((event) => event.length);

  Future countWherePlaylist(id) async =>
      (await (select(playlists)..where((t) => t.playlist_id.equals(id))).get())
          .length;

  Future insertInbox(Inbox inbox) => into(inboxs).insert(inbox);
  Future updateInbox(Inbox inbox) => update(inboxs).replace(inbox);
  Future deleteInbox(Inbox inbox) => delete(inboxs).delete(inbox);

  Future insertDownloadedSong(DownloadedSongData downloadedSong) =>
      into(this.downloadedSong).insert(downloadedSong);

  Stream<DownloadedSongData> getDownloadedSongById(int id) =>
      (select(downloadedSong)..where((tbl) => tbl.songId.equals(id)))
          .watchSingle();

  Stream<List<SongModel>> watchSongWithDownloadedSong() {
    final query = select(downloadedSong).join(
        [leftOuterJoin(songs, songs.idMusic.equalsExp(downloadedSong.songId))]);
    return query.watch().map((rows) => rows.map((row) {
          return SongModel.fromJson(jsonDecode(
            row.readTable(songs).data,
          ));
        }).toList());
  }

  Stream<List<AudioIbadahModel>> getDownloadedIbadah() {
    final query = select(downloadedIbadah).join(
        [leftOuterJoin(ibadahs, ibadahs.ibadahId.equalsExp(downloadedIbadah.ibadahId))]);
    return query.watch().map((rows) => rows.map((row) {
      return AudioIbadahModel.fromJson(jsonDecode(
        row.readTable(ibadahs).data,
      ));
    }).toList());
  }

  Stream<List<AudioBookModel>> getDownloadedBook() {
    final query = select(downloadedBook).join(
        [leftOuterJoin(audioBooks, audioBooks.bookId.equalsExp(downloadedBook.bookId))]);
    return query.watch().map((rows) => rows.map((row) {
      return AudioBookModel.fromJson(jsonDecode(
        row.readTable(audioBooks).data,
      ));
    }).toList());
  }

  Future insertDownloadedIbadahPart(DownloadedIbadahPartData data) async {
    final query = select(downloadedIbadahPart)
      ..where((tbl) => tbl.partId.equals(data.partId));
    final listPart = await query.get();
    if (listPart.isEmpty) {
      print('NEW PART BOOK IBADAH');
      into(downloadedIbadahPart).insert(data);
    } else {
      print('BOOK IBADAH PART EXIST AT DB');
    }
  }

  Stream<bool> isIbadahPartDownloaded(int ibadahId, int partId) {
    final query = select(downloadedIbadahPart)
      ..where((tbl) => tbl.ibadahId.equals(ibadahId));
    query.where((tbl) => tbl.partId.equals(partId));
    return query.watch().map((event) => event.isNotEmpty);
  }

  Future insertDownloadedBookIbadah(DownloadedIbadahData data) =>
      into(this.downloadedIbadah).insert(data);

  Stream<bool> isIbadahBookDownloaded(int id) =>
      ((select(downloadedIbadah)..where((t) => t.ibadahId.equals(id)))
          .watch().map((event) => event.isNotEmpty));

  Future insertDownloadedBook(DownloadedBookData data) =>
      into(this.downloadedBook).insert(data);

  Future insertDownloadedBookPart(DownloadedBookPartData data) async {
    final query = select(downloadedBookPart)
      ..where((tbl) => tbl.partId.equals(data.partId));
    final listPart = await query.get();
    if (listPart.isEmpty) {
      print('NEW PART BOOK');
      into(downloadedBookPart).insert(data);
    } else {
      print('BOOK PART EXIST AT DB');
    }
  }

  Stream<bool> isBookPartDownloaded(int bookId, int partId) {
    final query = select(downloadedBookPart)
      ..where((tbl) => tbl.bookId.equals(bookId));
    query.where((tbl) => tbl.partId.equals(partId));
    return query.watch().map((event) => event.isNotEmpty);
  }

  Stream<bool> isBookBookDownloaded(int id) =>
      (select(downloadedBook)..where((t) => t.bookId.equals(id)))
          .watch()
          .map((event) => event.isNotEmpty);
}
