import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:music_mulai/bloc/launcher/splash/splash_bloc.dart';
import 'package:music_mulai/util/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:music_mulai/model/song_service/song_list_model.dart'
    as SongsModel;

import '../../../main.dart';

class SplashLayout extends StatefulWidget {
  @override
  _SplashLayoutState createState() => _SplashLayoutState();
}

class _SplashLayoutState extends State<SplashLayout> {
  String versionCode = '0';
  String deviceCode = '0';
  // List<dynamic> songs;
  List<SongsModel.Song> songs;

  @override
  void initState() {
    BlocProvider.of<SplashBloc>(context).add(SplashInit());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) async {
        if (state is SplashGetDataSuccess) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => MyHomePage(0)));
        } else if (state is HitApiFailed) {
          if (state.isConnected) {
            GlobalComponent.feedbackFlushbar(
                context, state.errorMessage + ": Silahkan restart aplikasi.");
          } else {
            GlobalComponent.feedbackFlushbar(context, state.errorMessage);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => MyHomePage(0)));
          }
        }
      },
      child:
          BlocBuilder<SplashBloc, SplashState>(builder: (context, splashState) {
        return Stack(
          children: <Widget>[
            Container(
              color: Colors.orange,
              height: double.infinity,
              width: double.infinity,
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(bottom: 50.0),
                      child: Text(
                        "Loading..",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 325 / 2,
                height: 394 / 2,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: new AssetImage("assets/logo.png"),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
