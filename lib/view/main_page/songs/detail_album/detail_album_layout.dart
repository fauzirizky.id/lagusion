import 'package:flutter/material.dart';
import 'package:music_mulai/model/search_model/search_album_model.dart'
    as SearchAlbumModel;

class DetailAlbumLayout extends StatefulWidget {
  final SearchAlbumModel.Album album;

  DetailAlbumLayout(this.album);

  @override
  _DetailAlbumLayoutState createState() => _DetailAlbumLayoutState();
}

class _DetailAlbumLayoutState extends State<DetailAlbumLayout> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: _viewAppBar(),
      body: Container(
        width: double.infinity,
        child: _viewListSongs(),
      ),
    );
  }

  //View Layout - Begin
  Widget _viewListSongs() => Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10, top: 20),
              child: ListView.builder(
                  itemCount: widget.album.songs.length,
                  itemBuilder: (context, int index) {
                    return Container(
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                          color: Colors.grey.withOpacity(0.4),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        child: Image.network(
                                          widget.album.songs[index].thumbnail,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Flexible(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.6,
                                                child: Text(
                                                  (index + 1).toString() +
                                                      '. ' +
                                                      widget.album.songs[index]
                                                          .title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                                  .brightness
                                                                  .toString() ==
                                                              'Brightness.dark'
                                                          ? Colors.white
                                                          : Colors.black,
                                                      fontSize: 16.5,
                                                      fontFamily: 'Roboto'),
                                                ),
                                              ),
                                              SizedBox(height: 5.0),
                                              Text(
                                                widget
                                                    .album.songs[index].artist,
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                              .brightness
                                                              .toString() ==
                                                          'Brightness.dark'
                                                      ? Colors.white
                                                      : Colors.black
                                                          .withOpacity(0.5),
                                                  fontSize: 16.5,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'C 4/4',
                                                    style:
                                                        TextStyle(fontSize: 10),
                                                  ),
                                                  Container(
                                                    transform: Matrix4
                                                        .translationValues(
                                                            -8, 0, 0),
                                                    child: Icon(
                                                      Icons.play_arrow,
                                                      color: Theme.of(context)
                                                                  .brightness
                                                                  .toString() ==
                                                              'Brightness.dark'
                                                          ? Colors.white
                                                          : Colors.black
                                                              .withOpacity(0.6),
                                                      size: 25.0,
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Column(
                                                children: <Widget>[
                                                  Container(
                                                    transform: Matrix4
                                                        .translationValues(
                                                            8.0, 0.0, 0.0),
                                                    child:
                                                        PopupMenuButton<String>(
                                                      child: Icon(
                                                          Icons.more_vert,
                                                          size: 18),
                                                      onSelected:
                                                          (choice) async {},
                                                      itemBuilder: (BuildContext
                                                          context) {},
                                                    ),
                                                  ),
                                                  Text(widget.album.songs[index]
                                                      .duration
                                                      .replaceAll(
                                                          new RegExp(
                                                              r"\s+\b|\b\s"),
                                                          ""))
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Divider(),
                          ],
                        ));
                  }),
            ),
          ),
        ],
      );

  _viewAppBar() => PreferredSize(
        preferredSize: Size.fromHeight(65.0), // here the desired height
        child: AppBar(
          leading: Padding(
              padding: EdgeInsets.only(top: 10),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: BackButton())),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            transform: Matrix4.translationValues(-20.0, 0.0, 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 35.0,
                      width: 35.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          widget.album.thumbnail,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(left: 5),
                  width: MediaQuery.of(context).size.width - 180,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.album.albumName.toString(),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => SearchPage()));
              },
              icon: Icon(Icons.search),
            )
          ],
        ),
      );
  //View Layout - End
}
