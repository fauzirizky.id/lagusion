import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:music_mulai/bloc/main_page/songs/songs_bloc.dart';
import 'package:music_mulai/model/search_model/search_album_model.dart'
    as SearchAlbumModel;
import 'package:music_mulai/view/main_page/songs/detail_album/detail_album_layout.dart';

import '../../../detail/lagu/detail_tabLagu.dart';

class SongsLayout extends StatefulWidget {
  @override
  _SongsLayoutState createState() => _SongsLayoutState();
}

class _SongsLayoutState extends State<SongsLayout> {
  List<SearchAlbumModel.Album> _albums;
  bool _onLoad = false;

  var value = 0.1;

  @override
  void initState() {
    _getAlbums("");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SongsBloc, SongsState>(
      listener: (context, state) {
        if (state is SongsLoading) {
          _onLoad = true;
        } else if (state is HitApiFailed) {
          Flushbar(
            isDismissible: true,
            flushbarPosition: FlushbarPosition.TOP,
            duration: Duration(seconds: 3),
            backgroundColor: Colors.orange,
            message: state.errorMessage,
          )..show(context);
        } else if (state is SongsGetAlbumsSuccess) {
          setState(() {
            _onLoad = false;
            _albums = state.albums;
          });
        }
      },
      child: BlocBuilder<SongsBloc, SongsState>(builder: (context, songsState) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: double.infinity,
            child: RefreshIndicator(
              child: _onLoad
                  ? myCircularProgressIndicator()
                  : ListView(children: <Widget>[_viewListAlbums()]),
              onRefresh: () async {
                setState(() {
                  _onLoad = true;
                  _getAlbums("");
                });
              },
            ),
          ),
        );
      }),
    );
  }

  //View Layout - Begin
  Widget _viewListAlbums() => Padding(
        padding: EdgeInsets.only(left: 15, right: 10, top: 25),
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 2.4 / 3,
          ),
          itemCount: _albums == null ? 0 : _albums.length,
          itemBuilder: (context, index) {
            return Wrap(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                DetailAlbumLayout(_albums[index])));
                  },
                  child: _listCardAlbums(index),
                ),
                SizedBox(
                  width: 15,
                ),
              ],
            );
          },
        ),
      );

  Widget _listCardAlbums(int i) => Container(
        width: MediaQuery.of(context).size.width / 3.7,
        // height: MediaQuery.of(context).size.height / 5,
        padding: EdgeInsets.only(bottom: 10),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color:
                  Theme.of(context).brightness.toString() == 'Brightness.dark'
                      ? Colors.black
                      : Colors.grey.withOpacity(0.5),
              blurRadius: 10.0, // has the effect of softening the shadow
              spreadRadius: 0.0, // has the effect of extending the shadow
              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
          color: Theme.of(context).brightness.toString() == 'Brightness.dark'
              ? Color(0xff4d4d4d)
              : Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 8,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: new NetworkImage(_albums[i].thumbnail),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                  ),
                ),
                Positioned(
                  bottom: 5,
                  right: 5,
                  child: Icon(
                    Icons.play_circle_filled,
                    color: Theme.of(context).brightness.toString() ==
                            'Brightness.dark'
                        ? Colors.black
                        : Colors.white,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 5),
                  child: Text(
                    _albums[i].albumName,
                    style: TextStyle(fontSize: 13 + (5 * value)),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                PopupMenuButton<String>(
                  child: Icon(Icons.more_vert, size: 15),
                  onSelected: (choice) async {
                    // if (choice == MenuMore.LaguUtama) {
                    //   SharedPreferences prefs =
                    //       await SharedPreferences.getInstance();
                    //   prefs.setString('lagu_utama', widget.title);
                    //   prefs.commit();
                    // }
                  },
                  itemBuilder: (BuildContext context) {
                    // return MenuMore.choices.map((String choice) {
                    //   return PopupMenuItem<String>(
                    //     value: choice,
                    //     child: Text(choice),
                    //   );
                    // }).toList();
                  },
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Text(
                  _albums[i].songs.length.toString() + ' Siap diunduh',
                  style: TextStyle(fontSize: 10 + (5 * value)),
                  overflow: TextOverflow.ellipsis,
                )),
          ],
        ),
      );
  //View Layout - End

  //View Utilities - Begin
  myCircularProgressIndicator() => Center(
        child: CircularProgressIndicator(),
      );
  //View Utilities - End

  //Form Function - Begin
  //Form Function - End

  //Get Function - Begin
  _getAlbums(String albumName) {
    _sendEvent(SongsGetAlbums(albumName: albumName));
  }

  _sendEvent(eventName) {
    BlocProvider.of<SongsBloc>(context).add(eventName);
  }
  //Get Function - End

  //Post Function - Begin
  //Post Function - End

  //Process Function - Begin
  //Process Function - End

  //Show Function - Begin
  //Show Function - End

}
